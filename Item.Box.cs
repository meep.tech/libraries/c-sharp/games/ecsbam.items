﻿using System;
using Meep.Tech.Data.Base;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Meep.Tech.Collections;
using Meep.Tech.Data.Static;
using Meep.Tech.Games.Items.Inventories;
using Meep.Tech.Data.Serialization;
using Meep.Tech.Data;
using Meep.Tech.Data.Attributes.Models;

namespace Meep.Tech.Games.Items {

  public partial class Item {

    /// <summary>
    /// The base interface for boxes of an kind.
    /// </summary>
    public interface IBox : IInventoryMultipleItemContainer {

      /// <summary>
      /// Params for the base item construction
      /// </summary>
      public static class Params {

        /// <summary>
        /// Used internally for loading and unloading boxes.
        /// </summary>
        public static Item.Params BoxId {
          get;
        } = new Item.Params("BoxId", typeof(string));

        /// <summary>
        /// Helps remember what the maximum size of the box is
        /// </summary>
        public static Item.Params MaxSize {
          get;
        } = new Item.Params("BoxMaxSize", typeof(string));
      }

      /// <summary>
      /// Used to internally access the contents.
      /// </summary>
      internal QueuedCollection<IInventoryItem> _contents {
        get;
      }

      /// <summary>
      /// Try to add the contents of one box to another.
      /// </summary>
      bool takeContents(IBox other, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems);

      /// <summary>
      /// Try to remove items:
      /// </summary>
      IEnumerable<IInventoryItem> remove(IInventoryItem item, int quantity = int.MaxValue);

      Serializer.SerializedData ISerializeable.serialize(bool andAllChildrenRecursively) {
        throw new NotImplementedException();
      }

      ISerializeable ISerializeable.deserialize(Serializer.SerializedData serializedData, int? depthLimit) {
        throw new NotImplementedException();
      }
    }

    /// <summary>
    /// Limited type box
    /// </summary>
    /// <typeparam name="TLimitTo"></typeparam>
    [DataModel("Item", typeof(Item.Box))]
    public struct Box<TLimitTo> : IBox {

      /// <summary>
      /// Internal box to keep logic the same.
      /// </summary>
      Box _internal;

      /// <summary>
      /// Internal accessor
      /// </summary>
      QueuedCollection<IInventoryItem> IBox._contents {
        get => _internal._contents;
      }

      /// <summary>
      /// The box type as the item type
      /// </summary>
      public Item.Type type
        => _internal.type;

      /// <summary>
      /// Total number of items in this box:
      /// </summary>
      public int quantity {
        get => _internal.quantity;
        internal set => _internal.quantity = value;
      }
      int IInventoryMultipleItemContainer.quantity {
        get => quantity;
        set => throw new NotImplementedException("Cannot set the Quantity of a Box directly");
      }

      /// <summary>
      /// TODO: actually impliment this, boxes should have their own ids
      /// </summary>
      string IUnique.id {
        get => _internal.First().getUniqueId();
        set => throw new NotImplementedException("Cannot set the Item's ID via a Box");
      }

      /// <summary>
      /// TODO: actually impliment this, boxes should have their own owner value
      /// </summary>
      string IOwned.owner {
        get => _internal.First().getOwnerId();
        set => throw new NotImplementedException("Cannot set the Item's Owner via a Box");
      }

      /// <summary>
      /// The max quantity this box can hold.
      /// Boxes can change size pretty easily unlike stacks
      /// </summary>
      public int maxQuantity
        => _internal.maxQuantity;

      /// <summary>
      /// If the box is full
      /// </summary>
      public bool isFull
        => quantity >= maxQuantity; 

      /// <summary>
      /// how much quantity this can hold more.
      /// </summary>
      public int remainingSpace
        => maxQuantity - quantity; 

      /// <summary>
      /// The name of the first item in the box
      /// </summary>
      public string name
        => _internal._overrideName ?? _internal.First().name;

      /// <summary>
      /// Make a new stack for a given item
      /// </summary>
      public Box(IEnumerable<IInventoryItem> items = null, int? maxQuantity = null) {
        _internal = new Box(null, maxQuantity);
        if (!(items is Box<TLimitTo>)) {
          validate(items);
        }
        _internal.tryToAdd(items, out _, out _, out _);
      }

      void validate(IEnumerable<IInventoryItem> items) {
        if (items.Any(item => !(item is IBox box) ? !(item.type is TLimitTo) : box.Any(item => !(item.type is TLimitTo)))) {
          throw new ArrayTypeMismatchException($"Box Limited to Item Types: {typeof(TLimitTo).FullName}, tried to add incorrect item type.");
        }
      }

      public bool takeContents(Box<TLimitTo> other, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems) {
        return _internal.takeContents(other, out leftoverItems, out successfullyAddedItems);
      }

      public IInventoryItem clone(bool newUniqueId = true, string newUniqueIdToken = null)
        => new Box<TLimitTo>(_internal.Select(item => item.clone()), maxQuantity);

      public IEnumerator<IInventoryItem> GetEnumerator()
        => _internal.GetEnumerator();

      public bool takeContents(IBox other, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems) {
        if (!(other is Box<TLimitTo>)) {
          validate(other);
        }
        return _internal.takeContents(other, out leftoverItems, out successfullyAddedItems);
      }

      public bool tryToAdd(IEnumerable<IInventoryItem> items, out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems) {
        if (!(items is Box<TLimitTo>)) {
          validate(items);
        }
        return _internal.tryToAdd(items, out leftoverQuantity, out leftoverItems, out successfullyAddedItems);
      }

      public bool tryToAdd(out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems, params IInventoryItem[] items) {
        validate(items);
        return _internal.tryToAdd(items, out leftoverQuantity, out leftoverItems, out successfullyAddedItems);
      }

      public bool tryToTakeContents(IInventoryMultipleItemContainer other, out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems) {
        if (!(other is Box<TLimitTo>)) {
          validate(other);
        }
        return _internal.tryToTakeContents(other, out leftoverQuantity, out leftoverItems, out successfullyAddedItems);
      }

      public IEnumerable<IInventoryItem> remove(IInventoryItem item, int quantity = int.MaxValue)
        => _internal.remove(item, quantity);

      public IInventoryMultipleItemContainer split(int desiredResultQuantity) {
        int quantityToRemove = desiredResultQuantity > quantity ? quantity : quantity - desiredResultQuantity;
        return new Box<TLimitTo>(_internal.split(quantityToRemove), maxQuantity);
      }

      IEnumerator IEnumerable.GetEnumerator()
        => GetEnumerator();

      IInventoryMultipleItemContainer IInventoryMultipleItemContainer.addAndExpandWith(IInventoryItem inventoryItem)
        => (_internal as IInventoryMultipleItemContainer).addAndExpandWith(inventoryItem);
    }

    /// <summary>
    /// A stack of multiple different types of items
    /// </summary>
    /// </summary>
    [DataModel("Item", typeof(Item.Box))]
    public struct Box : IBox {

      /// <summary>
      /// If the box is full
      /// </summary>
      public bool isFull
        => quantity >= maxQuantity;

      /// <summary>
      /// Type for just a box.
      /// Kind of a placeholder
      /// </summary>
      public class Type : Item.Type {
        public new static Item.Type.Id Id {
          get;
        } = new Id("Box", "Collections.");
        Type() 
          : base(Id) {}

        protected override Item InitializeModel(Model.Params @params = null)
          => throw new NotImplementedException("Cannot create a Box with Make, use the ctor instead");
      }

      /// <summary>
      /// The actual contents, this should contain no nulls
      /// </summary>
      QueuedCollection<IInventoryItem> IBox._contents {
        get => _contents;
      } internal QueuedCollection<IInventoryItem> _contents;

      /// <summary>
      /// The name of the first item in the box
      /// </summary>
      public string name
        => _overrideName ?? _contents.First().name;

      /// <summary>
      /// A name to use instead of the basic one
      /// </summary>
      internal string _overrideName;

      /// <summary>
      /// The box type as the item type
      /// </summary>
      public Item.Type type {
        get;
      }

      /// <summary>
      /// The max quantity this box can hold.
      /// Boxes can change size pretty easily unlike stacks
      /// </summary>
      public int maxQuantity {
        get;
        private set;
      }

      /// <summary>
      /// how much quantity this can hold more.
      /// </summary>
      public int remainingSpace
        => maxQuantity - quantity;

      /// <summary>
      /// Total number of items in this box:
      /// </summary>
      public int quantity {
        get;
        internal set;
      } int IInventoryMultipleItemContainer.quantity {
        get => quantity;
        set => throw new NotImplementedException("Cannot set the Quantity of a Box directly");
      }

      /// <summary>
      /// TODO: actually impliment this, boxes should have their own ids
      /// </summary>
      string IUnique.id {
        get => _contents.First().getUniqueId();
        set => throw new NotImplementedException("Cannot set the Item's ID via a Box");
      }


      /// <summary>
      /// TODO: actually impliment this, boxes should have their own owner value
      /// </summary>
      string IOwned.owner {
        get => _contents.First().getOwnerId();
        set => throw new NotImplementedException("Cannot set the Item's Owner via a Box");
      }

      /// <summary>
      /// Make a new stack for a given item
      /// </summary>
      public Box(IEnumerable<IInventoryItem> items = null, int? maxQuantity = null, string name = null) : this() {
        _overrideName = name ?? _overrideName;
        type = Archetypes.GetInstance<Box.Type>();
        _contents = items?.Where(item => item != null)?.ToListQueue() ?? new QueuedCollection<IInventoryItem>();
        this.maxQuantity = maxQuantity ?? items.Count();
        calculateQuantity();
      }

      void calculateQuantity() {
        quantity = _contents.Sum(item => item is Stack stack ? stack.quantity : 1);
      }

      /// <summary>
      /// Take the contents of another item contaner and add it to this one.
      /// </summary>
      public bool tryToTakeContents(IInventoryMultipleItemContainer other, out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems) {
        if (other is Box box) {
          bool allAdded = takeContents(box, out leftoverItems, out successfullyAddedItems);

          leftoverQuantity = leftoverItems.Count();
          calculateQuantity();
          return allAdded;
        } // If it's not a box, add it's items as a sub item.
        // any leftovers are returned to the other (assumed stack) as a quantity:
        else {
          return tryToAdd(
            out leftoverQuantity,
            out leftoverItems,
            out successfullyAddedItems,
            other
          );
        }
      }

      /// <summary>
      /// Try to add the contents of one box to another.
      /// </summary>
      public bool takeContents(IBox other, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems) {
        QueuedCollection<IInventoryItem> otherItemsToAdd = other._contents;
        List<IInventoryItem> addedItems = new List<IInventoryItem>();
        List<IInventoryItem> leftovers = new List<IInventoryItem>();
        while(otherItemsToAdd.Any() && (maxQuantity - quantity) > 0) {
          IInventoryItem otherItem = otherItemsToAdd.Dequeue();
          if (!tryToAdd(out _, out var leftoversThisTime, out var addedThisTime, otherItem)) {
            leftovers.AddRange(leftoversThisTime);
          }
          if (addedThisTime != null) {
            addedItems.AddRange(addedThisTime);
          }
        }

        leftoverItems = otherItemsToAdd;
        successfullyAddedItems = addedItems;
        return leftoverItems.Count() > 0;
      }

      /// <summary>
      /// Add an item to the box.
      /// Items just get added, Item.Stacks get added and record quantity added.
      /// Boxes fail to add, can't put a box in a box.
      /// </summary>
      public bool tryToAdd(IEnumerable<IInventoryItem> items, out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems) {
        List<IInventoryItem> leftovers = new List<IInventoryItem>();
        List<IInventoryItem> addedItems = new List<IInventoryItem>();

        leftoverQuantity = 0;
        bool allAdded = true;
        leftoverItems = leftovers;
        foreach(var item in items) {
          if (item is Stack stack) {
            int remainingSpace;
            if ((remainingSpace = maxQuantity - quantity) > 0) {
              Stack stackRemaining = stack;
              // go though any existing stacks and try to add the items:
              foreach (Item.Stack existingStack in _contents.Where(cItem => cItem is Item.Stack itemsStack && itemsStack.containsTheSameItemsAs(item))) {
                if (remainingSpace <= stackRemaining.quantity) {
                  int quantityToAdd = existingStack.quantity;
                  if (!existingStack.tryToTakeContents(stackRemaining, out int notAddedQuantity, out var leftover, out var added)) {
                    quantityToAdd -= notAddedQuantity;
                    if (leftovers != null) {
                      stackRemaining = new Stack(leftover.First() as Item, leftover.Count());
                    }
                  }
                  if (added != null) {
                    addedItems.AddRange(added);
                  }

                  quantity += quantityToAdd;
                } // partial remaining space, just add what fits:
                else {
                  int quantityToAdd = existingStack.quantity;
                  if (!existingStack.tryToTakeContents(
                    new Stack(stackRemaining._baseItem.clone(), remainingSpace),
                    out int notAddedQuantity,
                    out var leftover,
                    out var added
                  )) {
                    quantityToAdd -= notAddedQuantity;
                  }
                  if (added != null) {
                    addedItems.AddRange(added);
                  }
                  stackRemaining.quantity -= quantityToAdd;
                  quantity = maxQuantity;
                  break;
                }
              }

              // if we still have a bit of stack to add, try to just toss it in:
              if (stackRemaining.quantity > 0) {
                // if there's space left:
                if ((remainingSpace = maxQuantity - quantity) > 0) {
                  if (remainingSpace <= stack.quantity) {
                    _contents.Add(stack);
                    addedItems.Add(stack);
                    quantity += stack.quantity;
                  } // partial remaining space, just add what fits:
                  else {
                    IInventoryMultipleItemContainer newStack = stack.split(remainingSpace);
                    _contents.Add(newStack);
                    quantity = maxQuantity;
                    leftoverQuantity += stack.quantity;
                    leftovers.Add(stack);
                    allAdded = false;
                  }
                } // no remaining space, just return the stack:
                else {
                  leftoverQuantity += stack.quantity;
                  leftovers.Add(stack);
                  allAdded = false;
                }
              }

              continue;
            } // no remaining space, just return the stack:
            else {
              leftoverQuantity += stack.quantity;
              leftovers.Add(stack);
              allAdded = false;
            }
          } else if (item is Box) throw new InvalidOperationException($"BOX WITHIN A BOX! Cannot add a Box to another Box as a sub-item.");

          if (maxQuantity - quantity > 0) {
            _contents.Add(item);
            addedItems.Add(item);
            quantity++;
          } else {
            leftoverQuantity++;
            leftovers.Add(item);
            allAdded = false;
          }
        }

        successfullyAddedItems = addedItems;
        return allAdded;
      }

      public bool tryToAdd(out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems, params IInventoryItem[] items)
        => tryToAdd(items, out leftoverQuantity, out leftoverItems, out successfullyAddedItems); 

      public IInventoryItem clone(bool newUniqueId = true, string newUniqueIdToken = null)
        => new Box(_contents.Select(item => item.clone()), maxQuantity);

      public IEnumerator<IInventoryItem> GetEnumerator()
        => _contents.GetEnumerator();

      IEnumerator IEnumerable.GetEnumerator()
        => GetEnumerator();

      public IEnumerable<IInventoryItem> remove(IInventoryItem itemToRemove, int quantity = int.MaxValue) {
        if (quantity == 0 || itemToRemove is IBox box) {
          return Enumerable.Empty<IInventoryItem>();
        }

        if (_contents.Contains(itemToRemove)) {
          int foundQty = 0;
          IInventoryItem partialItem = null;
          IEnumerable<IInventoryItem> foundItems = _contents.RemoveEach(currentItem => {
            if (foundQty >= quantity) {
              // break:
              return null;
            }

            // if the current item is just the item we're looking for:
            if (currentItem?.Equals(itemToRemove) ?? false) {
              foundQty++;
              return true;
            } // if it's a stack with a type matching the item
            else if (currentItem is Item.Stack stack && stack.First().Equals(itemToRemove)) {
              int potentialQuantity = stack.quantity + foundQty;
              // if the stack would put us over what we want, take some and leave the previous item in it's old place:
              if (potentialQuantity > quantity) {
                partialItem = stack.split(quantity - foundQty);
                return null;
              }// if it would be equal or less, just include it and increas the quantity 
              else {
                quantity += stack.quantity;
                return true;
              }
            } else return false;
          });

          return partialItem != null
            ? foundItems.Append(partialItem)
            : foundItems;

        } else return Enumerable.Empty<IInventoryItem>();
      }

      public IInventoryMultipleItemContainer split(int desiredResultQuantity) {
        int quantityToRemove = desiredResultQuantity > quantity 
          ? quantity 
          : quantity - desiredResultQuantity;
        IBox newBox = new Box() { maxQuantity = maxQuantity };

        while(quantityToRemove-- > 0) {
          newBox.tryToAdd(out _, out _, out _, _contents.Dequeue());
        }

        return newBox;
      }

      IInventoryMultipleItemContainer IInventoryMultipleItemContainer.addAndExpandWith(IInventoryItem inventoryItem) {
        int potentialQuantity = quantity + inventoryItem.getSlotQuantity();
        if (potentialQuantity > maxQuantity) {
          maxQuantity = potentialQuantity;
        }
        if (inventoryItem is IInventoryMultipleItemContainer container) {
          tryToTakeContents(container, out _, out _, out _);
        } else tryToAdd(out _, out _, out _, inventoryItem);
        return this;
      }
    }
  }
}