﻿using Meep.Tech.Data.Base;

namespace Meep.Tech.Games.Items.Inventories {
  public partial class Inventory {

    public partial class Type {

      public class Id : Identity {

        /// <summary>
        /// From nowhere, usually means an error or may be a brand new item.
        /// </summary>
        public static Id None {
          get;
        } = new Id("None");

        public Id(string name, string externalIdNameEnding = null)
          : base(name, $"Meep.Tech.Games.Items.Inventories.{externalIdNameEnding ?? name}") { }
      }
    }
  }
}
