﻿using Meep.Tech.Games.Basic.Geometry;
using System;
using System.Linq;
using System.Collections.Generic;
using Meep.Tech.Data;

namespace Meep.Tech.Games.Items.Inventories.Basic {

  /// <summary>
  /// A grid of items, with 0,0 at the bottom left.
  /// </summary>
  public class ItemGrid : Inventory {

    /// <summary>
    /// Item grid params
    /// </summary>
    public new class Params : Inventory.Params {

      /// <summary>
      /// The size in bounds (x,y) of the grid
      /// </summary>
      public static Params Bounds {
        get;
      } = new Params("Bounds", typeof(Coordinate));

      protected Params(string name, System.Type type) 
        : base(name, type, "Grid.") {}
    }

    /// <summary>
    /// The size bounds of this grid, x and y.
    /// 0,0 is bottom left
    /// </summary>
    public Coordinate bounds {
      get;
    }

    /// <summary>
    /// Item pivots
    /// </summary>
    protected IInventoryItem[,] _itemPivots;

    /// <summary>
    /// Try to get the next empty grid slot that fits the given item.
    /// This comes after we check for all duplicates, you don't need to account for that.
    /// </summary>
    protected virtual bool _tryToGetNextFreeInventoryGridSlotFor(IInventoryItem itemToFitIn, out Slot slot) {
      for (int y = _itemPivots.GetLength(1); y >= 0; y--) {
        for (int x = 0; x < _itemPivots.GetLength(0); x++) {
          slot = new Slot(type, (x, y), this.getUniqueId());
          if (_itemFitsInSlot(slot, itemToFitIn)) {
            return true;
          }
        }
      }

      slot = default;
      return false;
    }

    /// <summary>
    /// For a basic grid, any item fits in any empty slot.
    /// Can also change the slot if it should be placed elsewhere.
    /// This comes after we check for all duplicates, you don't need to account for that.
    /// </summary>
    protected virtual bool _itemFitsInSlot(Slot slot, IInventoryItem item)
      => _getItemAtPivot(slot) == null;

    /// <summary>
    /// Get the item at the given grid pivot location provided by the slot info.
    /// </summary>
    protected virtual IInventoryItem _getItemAtPivot(Slot slot)
      => _itemPivots[slot.gridLocation.x, slot.gridLocation.y];

    #region Inventory Base Implimentation

    protected override IEnumerable<(IInventoryItem item, Slot location)> slots {
      get {
        for (int y = _itemPivots.GetLength(1); y >= 0; y--) {
          for (int x = 0; x < _itemPivots.GetLength(0); x++) {
            Slot slot = new Slot(type, (x, y), this.getUniqueId());
            yield return (_getItemAtPivot(slot), slot);
          }
        }
      }
    }

    public ItemGrid(Coordinate gridBounds, Type archetype, string ownerId = null, string uniqueID = null)
      : base(archetype, ownerId, uniqueID) 
    {
      bounds = gridBounds;
      _itemPivots = new IInventoryItem[bounds.x, bounds.y];
    }

    protected override bool _isValidSlot(Slot slot)
      => slot.gridLocation.isWithin((0, 0), bounds);

    public override bool _contains(Slot slot, out IInventoryItem item) {
      item = _getItemAtPivot(slot);

      return true;
    }

    protected override bool _tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Slot> modifiedSlots) {
      leftover = item;
      successfullyAddedItem = null;
      List<Slot> slotsModified = new List<Slot>();
      foreach ((IInventoryItem currentItem, Slot slot) in slots) {
        if (currentItem is IInventoryMultipleItemContainer container) {
          // if we come to a container with room for the current item:
          if (!container.isFull && container.containsTheSameItemsAs(leftover, false)) {
            // try to add it:
            if (container.tryToAddOrTakeFrom(leftover, out _, out IEnumerable<IInventoryItem> newLeftovers, out IEnumerable<IInventoryItem> newAddedItems)) {
              // if we got new leftovers, replace:
              leftover = IInventoryItem.Combine(newLeftovers);
              // if we got new successfully added items, add them to the success results so far:
              if (newAddedItems != null && newAddedItems.Any()) {
                successfullyAddedItem = successfullyAddedItem?.combine(newAddedItems) ?? IInventoryItem.Combine(newAddedItems);
              }
              slotsModified.Add(slot);
            }

            if (leftover == null) {
              break;
            }
          }
        }
      }

      // if we didn't find slots that exist to add it to, add it to a new slot:
      if (leftover != null) {
        if (_tryToGetNextFreeInventoryGridSlotFor(leftover, out Slot addedToSlot)) {
          slotsModified.Add(addedToSlot);
          _itemPivots[addedToSlot.gridLocation.x, addedToSlot.gridLocation.y] = leftover;
          successfullyAddedItem = successfullyAddedItem?.combine(leftover) ?? leftover;
        } // if we failed to find a slot to add it to
        else {
          modifiedSlots = slotsModified;
          return false;
        }
      }

      modifiedSlots = slotsModified;
      return true;
    }

    protected override bool _tryToAdd(IInventoryItem item, Slot slot, bool slotIsOccupied, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem) {
      if (slotIsOccupied) {
        _itemPivots[slot.gridLocation.x, slot.gridLocation.y] =
          _getItemAtPivot(slot).combine(item);
      } else {
        _itemPivots[slot.gridLocation.x, slot.gridLocation.y] = item;
      }

      successfullyAddedItem = item;
      leftover = null;
      return true;
    }

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null)
      => new ItemGrid(bounds, type, this.getOwnerId(), newUniqueId ? null : this.getUniqueId()) {
        _itemPivots = _itemPivots
      };

    protected override IInventoryItem _removeAt(Slot slot, int quantity = int.MaxValue) {
      IInventoryItem found = _getItemAtPivot(slot);
      if (quantity < found.getSlotQuantity()) {
        found = (found as IInventoryMultipleItemContainer).split(quantity);
      } else {
        _itemPivots[slot.gridLocation.x, slot.gridLocation.z] = null;
      }

      return found;
    }

    public override IEnumerable<IInventoryItem> empty() {
      IInventoryItem[] tmp = new IInventoryItem[_itemPivots.GetLength(0) * _itemPivots.GetLength(1)];
      Buffer.BlockCopy(_itemPivots, 0, tmp, 0, tmp.Length * sizeof(int));
      _itemPivots = new IInventoryItem[bounds.x, bounds.y];

      return tmp;
    }

    #endregion

    public override IEnumerator<IInventoryItem> _getEnumerator()
      => (IEnumerator<IInventoryItem>)_itemPivots.GetEnumerator();
  }
}
