﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.Games.Items.Inventories.Basic {

  /// <summary>
  /// A simple list based inventory with indexes that re-orders on removals to avoid nulls and gaps.
  /// </summary>
  public class GaplessIndexedInventory : Inventory {

    List<IInventoryItem> _items;

    public GaplessIndexedInventory(Type archetype = null, int? quantityLimit = null, string ownerId = null, string uniqueId = null) 
      : base(archetype ?? Type.GetInstance<Type.None>(), ownerId, uniqueId) 
    {
      _items = quantityLimit.HasValue
        ? new List<IInventoryItem>(quantityLimit.Value)
        : new List<IInventoryItem>();
    }

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    public bool tryToAdd(IInventoryItem item, out int addedToIndex)
      => tryToAdd(item, out _, out addedToIndex);

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    public bool tryToAdd(IInventoryItem item, out IInventoryItem leftover, out int addedToIndex)
      => _tryToAdd(item, out leftover, out _, out addedToIndex);

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    protected virtual bool _tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out int addedToIndex) {
      bool success = _tryToAdd(item, out leftover, out successfullyAddedItem, out IEnumerable<Inventory.Slot> slot);
      if (success) {
        addedToIndex = slot.First().barSlotIndex;
      } else {
        addedToIndex = EmptyStackValue;
      }

      return success;
    }

    #region Abstract Inventory Implimentation

    protected override IEnumerable<(IInventoryItem item, Slot location)> slots 
      => _items.Select((index, item) => (item, new Inventory.Slot(index)));

    public override bool isFull
      => maxQuantity.HasValue ? quantity >= maxQuantity.Value : false;

    public override bool tryToSwap(Inventory.Slot inventorySlot, IInventoryItem newItem, out IInventoryItem oldItem) {
      // if there's nothing to swap:
      if (inventorySlot.barSlotIndex >= _items.Count) {
        oldItem = null;
        return false;
      }

      oldItem = _items[inventorySlot.barSlotIndex];
      _items[inventorySlot.barSlotIndex] = null;
      if (maxQuantity.HasValue && (quantity - 1) + newItem.getSlotQuantity() > maxQuantity) {
        _items[inventorySlot.barSlotIndex] = oldItem;
        return false;
      }

      _items[inventorySlot.barSlotIndex] = newItem;
      return true;
    }

    protected override bool _tryToAdd(IInventoryItem item, Slot slot, bool slotIsOccupied, out IInventoryItem leftover, out IInventoryItem succesfullyAddedItem) {
      if (slotIsOccupied) {
        _items[slot.barSlotIndex] 
          = _items[slot.barSlotIndex].combine(item);
      } else {
        _items.Insert(slot.barSlotIndex, item);
      }
      succesfullyAddedItem = item;
      leftover = null;
      return true;
    }

    protected override bool _tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Inventory.Slot> modifiedSlots) {
      _items.Add(successfullyAddedItem = item);
      modifiedSlots = new Slot(type, _items.Count - 1, this.getUniqueId()).AsEnumerable();
      leftover = null;

      return true;
    }

    public override bool _contains(Slot slot, out IInventoryItem item) {
      item = _items[slot.barSlotIndex];

      return true;
    }

    protected override IInventoryItem _removeAt(Slot slot, int quantity = int.MaxValue) {
      if (_items.Count > slot.barSlotIndex) {
        IInventoryItem found = tryToGet(slot);
        if (found is IInventoryMultipleItemContainer container) {
          found = container.getSlotQuantity() <= quantity
            ? found
            : container.split(quantity);
        }
        _items.RemoveAt(slot.barSlotIndex);

        return found;
      }

      return null;
    }

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null)
      => new GaplessIndexedInventory { _items = _items };

    public override bool tryToEmptyInto(IInventory otherInventory) {
      while (_items.Any() && !otherInventory.isFull) {
        var currentItem = _items[_items.Count - 1];
        if (otherInventory.tryToAdd(out _, currentItem)) {
          _items.RemoveAt(_items.Count - 1);
        }
      }

      return _items.Any();
    }

    public override IEnumerable<IInventoryItem> empty() {
      IEnumerable<IInventoryItem> contents = _items
        .Where(item => item != null);
      _items.Clear();

      return contents;
    }

    public override IEnumerator<IInventoryItem> _getEnumerator()
      => _items.GetEnumerator();

    #endregion
  }
}
