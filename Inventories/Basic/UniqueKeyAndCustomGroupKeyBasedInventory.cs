﻿using Meep.Tech.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.Games.Items.Inventories.Basic {

  /// <summary>
  /// An inventoy that not only uses the item unique key, but also groups items by a group key:
  /// </summary>
  /*public class UniqueKeyAndCustomGroupKeyBasedInventory<TCustomGroupKey> : Inventory {

    Dictionary<TCustomGroupKey, ICollection<string>> _uniqueKeysByGroupKey
      = new Dictionary<TCustomGroupKey, ICollection<string>>();

    Dictionary<string, IInventoryItem> _itemsByUniqueKey
      = new Dictionary<string, IInventoryItem>();

    public UniqueKeyAndCustomGroupKeyBasedInventory(
      Type archetype = null,
      string ownerId = null,
      string uniqueID = null
    ) : base(archetype, ownerId, uniqueID) {}

    /// <summary>
    /// Get all the items in one group as a read-only:
    /// </summary>
    public IEnumerable<IInventoryItem> this[TCustomGroupKey coordinate]
      => getGroup(coordinate);

    /// <summary>
    /// Get all the items in one group as a read-only:
    /// </summary>
    public IEnumerable<IInventoryItem> getGroup(TCustomGroupKey coordinate)
      => _uniqueKeysByGroupKey[coordinate].Select(id => _itemsByUniqueKey[id]);

    /// <summary>
    /// Add an item to a group
    /// </summary>
    public void add(TCustomGroupKey groupKey, IInventoryItem item)
      => tryToAdd(new Slot(item.id, type, groupKey), item, out _, out _);

    #region Inventory

    public override bool isFull 
      => false;

    protected override IEnumerable<(IInventoryItem item, Slot location)> slots
      => _uniqueKeysByGroupKey.SelectMany(
        entry => entry.Value.Select(
          itemId => 
            (_itemsByUniqueKey[itemId], new Slot(itemId, type, entry.Key))
        )
      );

    public override bool _contains(Slot slot, out IInventoryItem item)
      => _itemsByUniqueKey.TryGetValue(slot.stackKey as string, out item);
        

    protected override bool _tryToAdd(
      IInventoryItem item,
      out IInventoryItem leftover, 
      out IInventoryItem successfullyAddedItem, 
      out IEnumerable<Slot> modifiedSlots
    ) {
      _uniqueKeysByGroupKey.AddToValueCollection(default, item.getUniqueId());
      _itemsByUniqueKey.Add(item.getUniqueId(), successfullyAddedItem = item);
      leftover = null;
      modifiedSlots = new Slot(item.getUniqueId(), type, default(TCustomGroupKey)).AsEnumerable();
      return true;
    }

    protected override bool _tryToAdd(
      IInventoryItem item,
      Slot slot,
      bool slotIsOccupied,
      out IInventoryItem leftover,
      out IInventoryItem successfullyAddedItem
    ) {
      if (slotIsOccupied) {
        _itemsByUniqueKey[item.getUniqueId()] 
          = _itemsByUniqueKey[item.getUniqueId()].combine(item);
      } else {
        _uniqueKeysByGroupKey.AddToValueCollection((TCustomGroupKey)slot.otherKey, item.getUniqueId());
        _itemsByUniqueKey.Add(item.getUniqueId(), item);
      }

      successfullyAddedItem = item;
      leftover = null;
      return true;
    }

    public override IEnumerable<IInventoryItem> empty() {
      var @return = _itemsByUniqueKey.Values;
      _itemsByUniqueKey.Clear();
      _uniqueKeysByGroupKey.Clear();

      return @return;
    }

    protected override IInventoryItem _removeAt(Slot slot, int quantity = int.MaxValue) {
      string itemId = slot.stackKey as string ?? "";
      if (_itemsByUniqueKey.TryGetValue(itemId, out IInventoryItem item)) {
        if (quantity < item.getSlotQuantity()) {
          item = (item as IInventoryMultipleItemContainer).split(quantity);
        } else {
          _itemsByUniqueKey.Remove(itemId);
          _uniqueKeysByGroupKey.Values.ForEach(keys => keys.Remove(itemId));
        }

        return item;
      }

      return null;
    }

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null) {
      var newInventory = new UniqueKeyAndCustomGroupKeyBasedInventory<TCustomGroupKey>(
        type,
        newUniqueId
          ? RNG.GetNextUniqueIdFromToken(newUniqueIdToken)
          : this.getUniqueId()
      ) {
        _itemsByUniqueKey = _itemsByUniqueKey,
        _uniqueKeysByGroupKey = _uniqueKeysByGroupKey
      };

      return newInventory;
    }

    public override IEnumerator<IInventoryItem> _getEnumerator()
      => _itemsByUniqueKey.Values.GetEnumerator();

    #endregion
  }*/
}
