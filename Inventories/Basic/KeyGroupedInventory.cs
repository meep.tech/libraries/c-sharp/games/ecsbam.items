﻿using Meep.Tech.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Meep.Tech.Games.Items.Inventories.Basic {
  public class KeyGroupedInventory<TKey> : Inventory {

    Dictionary<TKey, SimpleIdBasedInventory> _items
      = new Dictionary<TKey, SimpleIdBasedInventory>();

    Func<IInventoryItem, TKey> _autoGetKeyFunction;

    protected override IEnumerable<(IInventoryItem item, Slot location)> slots
      => _items.SelectMany(entry => {
          TKey key = entry.Key;
          return (entry.Value as IEnumerable<IInventoryItem>)
            .Select(item => 
              (item, new Slot(type, key, this.getUniqueId()))
            );
        });

    public override IEnumerable<IInventoryItem> empty() {
      var items = _items.Values.SelectMany(inventory => inventory.Values);
      _items.Clear();

      return items;
    }

    public override bool _contains(Slot slot, out IInventoryItem item) {
      if (_items.TryGetValue((TKey)slot.stackKey, out var inventory)) {
        return inventory._contains(slot, out item);
      } else {
        item = null;
        return false;
      }
    }

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null)
      => new KeyGroupedInventory<TKey>(
        type,
        _autoGetKeyFunction,
        this.getOwnerId(),
        newUniqueId 
          ? RNG.GetNextUniqueIdFromToken(newUniqueIdToken) 
          : this.getUniqueId()
      ) {
        _items = _items
      };

    protected override IInventoryItem _removeAt(Slot slot, int quantity = int.MaxValue) {
      IInventoryItem found = tryToGet(slot);
      if (found is IInventoryMultipleItemContainer container) {
        if (container.getSlotQuantity() > quantity) {
          found = container.split(quantity);
        } else {
          _items[(TKey)slot.otherKey].removeAt(slot.barSlotIndex);
        }
      }

      return found;
    }

    protected override bool _tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Slot> modifiedSlots) {
      throw new NotImplementedException();
    }

    protected override bool _tryToAdd(IInventoryItem item, Slot slot, bool slotIsOccupied, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem) {
      throw new NotImplementedException();
    }

    public KeyGroupedInventory(
      Type archetype = null,
      Func<IInventoryItem, TKey> getKeyFromItemFunction = null,
      string ownerId = null, 
      string uniqueID = null
    ) : base(archetype, ownerId, uniqueID) { _autoGetKeyFunction = getKeyFromItemFunction; }
  }
}
