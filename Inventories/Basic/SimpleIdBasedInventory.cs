﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Meep.Tech.Games.Items.Inventories.Basic {

  /// <summary>
  /// A super simple bag of items. They don't auto combine or compress etc.
  /// </summary>
  public class SimpleIdBasedInventory
    : Inventory,
      IDictionary<string, IInventoryItem> {

    Dictionary<string, IInventoryItem> _items
      = new Dictionary<string, IInventoryItem>();

    /// <summary>
    /// Get an item by it's id without removing it.
    /// </summary>
    public IInventoryItem get(string itemId)
      => _items[itemId];

    /// <summary>
    /// Get an item by it's id without removing it.
    /// </summary>
    public bool tryToGet(string itemId, out IInventoryItem foundItem)
      => _items.TryGetValue(itemId, out foundItem);

    /// <summary>
    /// Check if this inventory has an entry for the given item.
    /// </summary>
    public override bool contains(IInventoryItem item)
      => _items.ContainsKey(item.id);

    /// <summary>
    /// Check if this inventory has an entry for the given item id
    /// </summary>
    public bool contains(string itemId)
      => _items.ContainsKey(itemId);

    /// <summary>
    /// Add an item to this simple inventory.
    /// </summary>
    public string add(IInventoryItem newDrop) {
      _items[newDrop.id] = newDrop;

      return newDrop.id;
    }

    /// <summary>
    /// Add an item to this simple inventory.
    /// </summary>
    public IInventoryItem remove(string itemId) {
      if (_items.TryGetValue(itemId, out IInventoryItem foundItem)) {
        _items.Remove(itemId);
        return foundItem;
      }

      return null;
    }

    #region Inventory

    public SimpleIdBasedInventory(IEnumerable<IInventoryItem> items, Type archetype = null, string ownerId = null, string uniqueID = null) 
      : this(archetype, ownerId, uniqueID) 
         => items.Select(add);

    public SimpleIdBasedInventory(Type archetype = null, string ownerId = null, string uniqueID = null) 
      : base(archetype, ownerId, uniqueID) {}

    protected override IEnumerable<(IInventoryItem item, Slot location)> slots 
      => _items.Values.Select(item => (item, new Slot(itemStackId: item.id, containingInventory: type)));

    protected override bool _tryToAdd(
      IInventoryItem item,
      out IInventoryItem leftover,
      out IInventoryItem successfullyAddedItem,
      out IEnumerable<Slot> modifiedSlots
    ) {
      successfullyAddedItem = _items[item.getUniqueId()] = item;
      leftover = null;
      modifiedSlots = new Slot(item.getUniqueId(), type).AsEnumerable();

      return true;
    }

    protected override bool _tryToAdd(
      IInventoryItem item,
      Slot slot,
      bool slotIsOccupied,
      out IInventoryItem leftover,
      out IInventoryItem successfullyAddedItem
    ) {
      if (slotIsOccupied) {
        _items[item.getUniqueId()]
          = _items[item.getUniqueId()].combine(item);
      } else {
        _items[item.getUniqueId()] = item;
      }
      successfullyAddedItem = _items[item.getUniqueId()] = item;
      leftover = null;

      return true;
    }

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null)
      => new SimpleIdBasedInventory(type, newUniqueId ? null : this.getUniqueId()) {
        _items = this._items
      };

    public override bool _contains(Slot slot, out IInventoryItem item)
      => _items.TryGetValue(slot.stackKey as string, out item);

    protected override IInventoryItem _removeAt(Slot slot, int quantity = int.MaxValue) {
      var item = tryToGet(slot);
      if (item != null) {
        if (quantity < item.getSlotQuantity()) {
          item = (item as IInventoryMultipleItemContainer).split(quantity);
        } else {
          _items.Remove(slot.stackKey as string);
        }
      }

      return item;
    }

    public override IEnumerable<IInventoryItem> empty() {
      var values = _items.Values;
      _items.Clear();
      return values;
    }

    public override IEnumerator<IInventoryItem> _getEnumerator()
      => _items.Values.GetEnumerator();

    #endregion

    #region IDictionary

    public IInventoryItem this[string key] {
      get => ((IDictionary<string, IInventoryItem>)_items)[key];
      set => ((IDictionary<string, IInventoryItem>)_items)[key] = value;
    }

    public ICollection<string> Keys => ((IDictionary<string, IInventoryItem>)_items).Keys;

    public ICollection<IInventoryItem> Values => ((IDictionary<string, IInventoryItem>)_items).Values;

    public int Count => ((ICollection<KeyValuePair<string, IInventoryItem>>)_items).Count;

    public bool IsReadOnly => ((ICollection<KeyValuePair<string, IInventoryItem>>)_items).IsReadOnly;

    public void Add(string key, IInventoryItem value) {
      ((IDictionary<string, IInventoryItem>)_items).Add(key, value);
    }

    public void Add(KeyValuePair<string, IInventoryItem> item) {
      ((ICollection<KeyValuePair<string, IInventoryItem>>)_items).Add(item);
    }

    public void Clear() {
      ((ICollection<KeyValuePair<string, IInventoryItem>>)_items).Clear();
    }

    public bool Contains(KeyValuePair<string, IInventoryItem> item) {
      return ((ICollection<KeyValuePair<string, IInventoryItem>>)_items).Contains(item);
    }

    public bool ContainsKey(string key) {
      return ((IDictionary<string, IInventoryItem>)_items).ContainsKey(key);
    }

    public void CopyTo(KeyValuePair<string, IInventoryItem>[] array, int arrayIndex) {
      ((ICollection<KeyValuePair<string, IInventoryItem>>)_items).CopyTo(array, arrayIndex);
    }

    public bool Remove(string key) {
      return ((IDictionary<string, IInventoryItem>)_items).Remove(key);
    }

    public bool Remove(KeyValuePair<string, IInventoryItem> item) {
      return ((ICollection<KeyValuePair<string, IInventoryItem>>)_items).Remove(item);
    }

    public bool TryGetValue(string key, out IInventoryItem value) {
      return ((IDictionary<string, IInventoryItem>)_items).TryGetValue(key, out value);
    }

    IEnumerator<KeyValuePair<string, IInventoryItem>> IEnumerable<KeyValuePair<string, IInventoryItem>>.GetEnumerator() {
      return ((IEnumerable<KeyValuePair<string, IInventoryItem>>)_items).GetEnumerator();
    }

    #endregion

  }
}
