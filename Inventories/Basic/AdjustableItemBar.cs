﻿using Meep.Tech.Data;
using Meep.Tech.Data.Attributes.Models;

namespace Meep.Tech.Games.Items.Inventories.Basic {

  /// <summary>
  /// An item bar made of a fixed # of item slots.
  /// </summary>
  public class AdjustableItemBar : ItemBar {

    #region Model Data

    /// <summary>
    /// Params for the model construction
    /// </summary>
    public new class Params : ItemBar.Params {

      /// <summary>
      /// The amount of slots in the item bar
      /// </summary>
      public static Param ActiveBarSlots {
        get;
      } = new Item.Params("Active Bar Slots", typeof(int?));

      protected Params(string name, System.Type type, string externalIdEnding = null)
        : base(name, type, $"Adjustable.{externalIdEnding ?? ""}") { }
    }

    #region Fields

    [ModelDataField]
    public int activeSlotCount {
      get;
      protected set;
    }

    #endregion

    #endregion

    /// <summary>
    /// Try to add a slot to the useable active ones in the bar.
    /// </summary>
    public bool tryToIncreaseActiveBarSlots() {
      if (activeSlotCount < maxSlotCount) {
        activeSlotCount++;
        return true;
      }

      return false;
    }

    /// <summary>
    /// Try to trim a slot from the useable active ones in the bar.
    /// </summary>
    public bool tryToDecreaseActiveBarSlots(out IInventoryItem trimmedItem) {
      if (activeSlotCount > 0) {
        trimmedItem = _items[activeSlotCount - 1] != null
          ? removeAt(new Slot((activeSlotCount - 1, 0)))
          : null;
        return true;
      }

      trimmedItem = null;
      return false;
    }

    #region Inventory Base Implimentation


    public AdjustableItemBar(int barSlotCount, int? activeSlots = null, Type type = null, string ownerId = null, string uniqueID = null)
      : base(barSlotCount, type, ownerId, uniqueID) 
    {
      activeSlotCount = activeSlots ?? maxSlotCount;
    }

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null) { 
      var duplicate = base._duplicate(newUniqueId, newUniqueIdToken) as AdjustableItemBar;
      duplicate.activeSlotCount = activeSlotCount;
      return duplicate;
    }

    protected override bool _isValidSlot(Slot slot)
      /// out of bounds
      => slot.barSlotIndex >= 0 && slot.barSlotIndex < activeSlotCount;

    /// <summary>
    /// Get the next empty slot index if one exists.
    /// </summary>
    /// <returns></returns>
    protected override int? getNextFreeSlotIndex()
      => this.FirstOrDefault(
        (item, index) => item == null
          ? (index < activeSlotCount ? (bool?)true : null, index)
          : (index < activeSlotCount ? (bool?)false : null, index));

    #endregion
  }
}
