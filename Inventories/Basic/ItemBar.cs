﻿using System.Collections.Generic;
using System.Linq;
using Meep.Tech.Data;
using Meep.Tech.Data.Attributes.Models;

namespace Meep.Tech.Games.Items.Inventories.Basic {

  /// <summary>
  /// An item bar made of a fixed # of item slots.
  /// </summary>
  public class ItemBar : Inventory {

    #region Model Data

    /// <summary>
    /// Params for the model construction
    /// </summary>
    public new class Params : Inventory.Params {

      /// <summary>
      /// The amount of slots in the item bar
      /// </summary>
      public static Param BarSlotCount {
        get;
      } = new Item.Params("Bar Slots", typeof(int?));

      protected Params(string name, System.Type type, string externalIdEnding = null)
        : base(name, type, $"Bar.{externalIdEnding ?? ""}") { }
    }

    #region Fields

    [ModelDataField]
    public int maxSlotCount {
      get;
    } int _nextFreeSlotIndex = 0;

    #endregion

    #endregion

    /// <summary>
    /// items by slot id
    /// </summary>
    protected IInventoryItem[] _items;

    /// <summary>
    /// Try to add to a specific bar slot
    /// </summary>
    public virtual bool tryToAdd(IInventoryItem item, int barSlot, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem)
      => tryToAdd(new Slot(barSlot), item, out leftover, out successfullyAddedItem);

    /// <summary>
    /// Get the next empty slot index if one exists.
    /// </summary>
    /// <returns></returns>
    protected virtual int? getNextFreeSlotIndex()
      => _items.FirstOrDefault(
        (item, index) => item == null 
          ? ((bool, int?)) (true, index) 
          : (false, index));

    #region Inventory Base Implimentation

    protected override IEnumerable<(IInventoryItem item, Slot location)> slots 
      => _items.Select((index, item) => (item, new Slot(type, (index, 0), this.getOwnerId())));

    public ItemBar(int barSlotCount, Inventory.Type type = null, string ownerId = null, string uniqueID = null) : base(type, ownerId, uniqueID) {
      maxSlotCount = barSlotCount + 1;
      _items = new IInventoryItem[maxSlotCount];
    }

    public override bool _contains(Slot slot, out IInventoryItem item) {
      item = _items[slot.barSlotIndex];

      return true;
    }

    public override IEnumerable<IInventoryItem> empty() {
      IInventoryItem[] items = _items.ToArray();
      _items = new IInventoryItem[maxSlotCount];

      return items;
    }

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null)
      => new ItemBar(maxSlotCount, type, newUniqueId ? null : this.getUniqueId()) {
        _items = _items,
        _nextFreeSlotIndex = _nextFreeSlotIndex
      };

    protected override IInventoryItem _removeAt(Slot slot, int quantity = int.MaxValue) {
      IInventoryItem found = tryToGet(slot);
      if (found == null) {
        return null;
      }
      if (quantity < found.getSlotQuantity()) {
        found = (found as IInventoryMultipleItemContainer).split(quantity);
      } else {
        _items[slot.barSlotIndex] = null;
        if (slot.barSlotIndex < _nextFreeSlotIndex) {
          _nextFreeSlotIndex = slot.barSlotIndex;
        }
      }

      return found;
    }

    protected override bool _tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Slot> modifiedSlots) {
      if (_nextFreeSlotIndex != EmptyStackValue) {
        _items[_nextFreeSlotIndex] = successfullyAddedItem = item;
        modifiedSlots = new Slot(type, (_nextFreeSlotIndex, 0), this.getOwnerId()).AsEnumerable();
        _nextFreeSlotIndex = getNextFreeSlotIndex() ?? EmptyStackValue;
        leftover = null;
        return true;
      }

      leftover = item;
      successfullyAddedItem = null;
      modifiedSlots = Enumerable.Empty<Slot>();
      return false;
    }

    protected override bool _tryToAdd(IInventoryItem item, Slot slot, bool slotIsOccupied, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem) {
      if (slotIsOccupied) {
        _items[slot.barSlotIndex]
          = _items[slot.barSlotIndex].combine(item);
      } else {
        _items[slot.barSlotIndex] = item;
      }
      successfullyAddedItem = item;
      leftover = null;

      if (slot.barSlotIndex == _nextFreeSlotIndex) {
        _nextFreeSlotIndex = getNextFreeSlotIndex() ?? EmptyStackValue;
      }  

      return true;
    }

    protected override bool _isValidSlot(Slot slot)
      /// out of bounds
      => slot.barSlotIndex >= 0 && slot.barSlotIndex < maxSlotCount;

    /// <summary>
    /// Overrideable for getenumerator.
    /// If this isn't overriden it uses the slot logic.
    /// </summary>
    /// <returns></returns>
    public override IEnumerator<IInventoryItem> _getEnumerator()
      => (IEnumerator<IInventoryItem>)_items.GetEnumerator();

    #endregion
  }
}
