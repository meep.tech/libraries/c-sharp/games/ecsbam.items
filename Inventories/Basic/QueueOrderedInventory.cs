﻿using Meep.Tech.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.Games.Items.Inventories.Basic {

  /// <summary>
  /// An inventory with an ordered set of items.
  /// </summary>
  /*public class QueueOrderedInventory : Inventory {

    QueuedCollection<IInventoryItem> _items
      = new QueuedCollection<IInventoryItem>();

    public QueueOrderedInventory(Type archetype = null, string ownerId = null, string uniqueId = null)
      : base(archetype ?? Type.GetInstance<Type.None>(), ownerId, uniqueId) { }

    protected override IEnumerable<(IInventoryItem item, Slot location)> slots
      => _items.Select(item => (item, new Slot()));

    public override bool isFull
      => false;

    public override IEnumerable<IInventoryItem> empty() {
      IInventoryItem[] contents = new IInventoryItem[_items.Count];
      _items.CopyTo(contents, 0);
      _items.Clear();

      return contents;
    }

    public override bool tryToEmptyInto(IInventory otherInventory) {
      List<IInventoryItem> leftovers = new List<IInventoryItem>();
      _items.RemoveEach(item => {
        if (otherInventory.tryToAdd(out IEnumerable<IInventoryItem> leftoverItem, item)) {
          return true;
        }

        return null;
      });

      return _items.Any();
    }

    public override IEnumerable<IInventoryItem> removeEachUntil(Func<IInventoryItem, (bool remove, bool @continue)> removeUntil = null, int quantity = int.MaxValue) {
      if (quantity == 0) {
        return Enumerable.Empty<IInventoryItem>();
      }

      int quantityToGrab = quantity;
      IInventoryItem partialItem = null;
      IEnumerable<IInventoryItem> foundItems = _items.RemoveEach(item => {
        bool remove, @continue;
        (remove, @continue) = removeUntil(item);
        if (remove) {
          if (item is IInventoryMultipleItemContainer container && quantityToGrab - container.getSlotQuantity() < 0) {
            // if we only removed part of a container, don't remove it from the inventory.
            partialItem = container.split(quantityToGrab);
            quantityToGrab = 0;
            @continue = false;
          } else {
            quantityToGrab -= item.getSlotQuantity();
            // returns true.
          }
        }

        if (!@continue) {
          return null;
        }

        return remove;
      });

      return partialItem != null
        ? foundItems.Append(partialItem)
        : foundItems;
    }

    public override IEnumerable<IInventoryItem> removeAll(IInventoryItem itemToRemove, int quantity = int.MaxValue) {
      if (quantity == 0) {
        return Enumerable.Empty<IInventoryItem>();
      }

      if (contains(itemToRemove)) {
        int foundQty = 0;
        IInventoryItem partialItem = null;
        IEnumerable<IInventoryItem> foundItems = _items.RemoveEach(currentItem => {
          if (foundQty >= quantity) {
            // break:
            return null;
          }

          // if the current item is just the item we're looking for:
          if (currentItem?.Equals(itemToRemove) ?? false) {
            foundQty++;
            return true;
          } // if the current item didn't equal the one to remove, but the one to remove is a container, we failed already, containers can't hold containers:
          else if (itemToRemove is IInventoryMultipleItemContainer) {
            return false;
          } // if the current item is a box, and it contains the item we're looking for:
          else if (itemToRemove is Item.IBox box && box.Contains(itemToRemove)) {
            IEnumerable<IInventoryItem> foundBoxItems = box.Where(i => i.Equals(itemToRemove));
            int potentialQuantity = foundBoxItems.Count() + foundQty;
            // if the box would put us over what we want, take some and leave the previous item in it's old place:
            if (potentialQuantity > quantity) {
              partialItem = new Item.Box(box.remove(itemToRemove, quantity - foundQty), box.maxQuantity);
              return null;
            } // if it would be equal or less, just include it and increas the quantity 
            else {
              quantity += foundBoxItems.Count();
              return true;
            }
          } // if it's a stack with a type matching the item
          else if (currentItem is Item.Stack stack && stack.First().Equals(itemToRemove)) {
            int potentialQuantity = stack.quantity + foundQty;
            // if the stack would put us over what we want, take some and leave the previous item in it's old place:
            if (potentialQuantity > quantity) {
              partialItem = stack.split(quantity - foundQty);
              return null;
            }// if it would be equal or less, just include it and increas the quantity 
            else {
              quantity += stack.quantity;
              return true;
            }
          } else return false;
        });

        return partialItem != null 
          ? foundItems.Append(partialItem)
          : foundItems;

      } else return Enumerable.Empty<IInventoryItem>();
    }

    public override IInventoryItem removeFirst(IInventoryItem itemToRemove, int quantity = int.MaxValue) {
      if (quantity != 0 && _items.Remove(itemToRemove, out IInventoryItem removedItem)) {
        return removedItem;
      }

      return null;
    }

    /// <summary>
    /// No matter what, the slot can't be dequeued for this type:
    /// </summary>
    protected override IInventoryItem _removeAt(Slot slot, int quantity = int.MaxValue)
      => _items.Dequeue();

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null) {
      throw new NotImplementedException();
    }

    protected override bool _tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Slot> modifiedSlot) {
      throw new NotImplementedException();
    }

    protected override bool _tryToAdd(IInventoryItem item, Slot slot, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem) {
      throw new NotImplementedException();
    }

    public override IEnumerator<IInventoryItem> _getEnumerator()
      => _items.GetEnumerator();
  }*/
}
