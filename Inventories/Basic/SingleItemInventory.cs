﻿using Meep.Tech.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Meep.Tech.Games.Items.Inventories.Basic {

  /// <summary>
  /// Only holds one item at slot 0
  /// </summary>
  public class SingleItemInventory : Inventory {

    /// <summary>
    /// The only slot in this inventory
    /// </summary>
    public Slot onlySlot 
      => new Slot(type, 0, this.getUniqueId());

    IInventoryItem _item;

    public SingleItemInventory(Type archetype, string ownerId = null, string uniqueID = null) 
      : base(archetype, ownerId, uniqueID) {}

    protected override IEnumerable<(IInventoryItem item, Slot location)> slots
      => (_item, onlySlot).AsEnumerable();

    public override IEnumerable<IInventoryItem> empty() {
      var item = _item;
      _item = null;

      return item.AsEnumerable();
    }

    protected override bool _isValidSlot(Slot slot)
      => slot.barSlotIndex == onlySlot.barSlotIndex;

    public override bool _contains(Slot slot, out IInventoryItem item)
      => (item = _item) != null;

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null)
      => new SingleItemInventory(type, this.getOwnerId(), newUniqueId ? RNG.GetNextUniqueIdFromToken(newUniqueIdToken) : this.getUniqueId()) {
        _item = _item
      };

    protected override IInventoryItem _removeAt(Slot slot, int quantity = int.MaxValue) {
      var item = _item;
      _item = null;

      return item;
    }

    protected override bool _tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Slot> modifiedSlots) {
      if (tryToAdd(onlySlot, item, out leftover, out successfullyAddedItem)) {
        modifiedSlots = onlySlot.AsEnumerable();
        return true;
      }

      modifiedSlots = Enumerable.Empty<Slot>();
      return false;
    }

    protected override bool _tryToAdd(IInventoryItem item, Slot slot, bool slotIsOccupied, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem) {
      if (slotIsOccupied) {
        _item = _item.combine(item);
      } else {
        _item = item;
      }

      successfullyAddedItem = item;
      leftover = null;
      return true;
    }
  }
}
