﻿using System;
using System.Linq;
using System.Collections.Generic;
using Meep.Tech.Data;

namespace Meep.Tech.Games.Items.Inventories.Basic {

  /// <summary>
  /// An item inventory that uses a key like a dictionary to store stackss
  /// </summary>
  public class ItemDictionary<TKey> : Inventory, IDictionary<TKey, IInventoryItem> {

    protected Dictionary<TKey, IInventoryItem> _items
      = new Dictionary<TKey, IInventoryItem>();

    #region Inventory

    protected override IEnumerable<(IInventoryItem item, Slot location)> slots 
      => _items.Select(entry => (entry.Value, new Slot(type, entry.Key, this.getUniqueId())));

    public ItemDictionary(Type archetype, string ownerId = null, string uniqueID = null) 
      : base(archetype, ownerId, uniqueID) {}

    public override IEnumerable<IInventoryItem> empty() {
      var items = _items.Values;
      _items.Clear();

      return items;
    }

    protected override bool _isValidSlot(Slot slot) 
      => base._isValidSlot(slot) && ((TKey)slot.stackKey != null);

    public override bool _contains(Slot slot, out IInventoryItem item)
      => _items.TryGetValue((TKey)slot.stackKey, out item);

    protected override Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null)
      => new ItemDictionary<TKey>(type, this.getOwnerId(), newUniqueId ? RNG.GetNextUniqueIdFromToken(newUniqueIdToken) : this.getUniqueId()) {
        _items = _items
      };

    protected override IInventoryItem _removeAt(Slot slot, int quantity = int.MaxValue) {
      if (contains(slot, out var item)) {
        if (quantity < item.getSlotQuantity()) {
          return (item as IInventoryMultipleItemContainer).split(quantity);
        } else {
          Remove((TKey)slot.stackKey);
          return item;
        }
      }

      return null;
    }

    /// <summary>
    /// Item Dictionaries cannot add items to the dictionary without a key
    /// </summary>
    protected override bool _tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Slot> modifiedSlots) {
      successfullyAddedItem = null;
      modifiedSlots = Enumerable.Empty<Slot>();
      leftover = item;

      return false;
    }

    protected override bool _tryToAdd(IInventoryItem item, Slot slot, bool slotIsOccupied, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem) {
      TKey slotKey = (TKey)slot.stackKey;
      if (slotIsOccupied) {
        _items[slotKey]
          = _items[slotKey].combine(item);
      } else {
        _items[slotKey] = item;
      }
      successfullyAddedItem = item;
      leftover = null;

      return true;
    }

    #endregion

    #region Dictionary

    public IInventoryItem this[TKey key] { get => ((IDictionary<TKey, IInventoryItem>)_items)[key]; set => ((IDictionary<TKey, IInventoryItem>)_items)[key] = value; }

    public ICollection<TKey> Keys => ((IDictionary<TKey, IInventoryItem>)_items).Keys;

    public ICollection<IInventoryItem> Values => ((IDictionary<TKey, IInventoryItem>)_items).Values;

    public int Count => ((ICollection<KeyValuePair<TKey, IInventoryItem>>)_items).Count;

    public bool IsReadOnly => ((ICollection<KeyValuePair<TKey, IInventoryItem>>)_items).IsReadOnly;

    public void Add(TKey key, IInventoryItem value) {
      ((IDictionary<TKey, IInventoryItem>)_items).Add(key, value);
    }

    public void Add(KeyValuePair<TKey, IInventoryItem> item) {
      ((ICollection<KeyValuePair<TKey, IInventoryItem>>)_items).Add(item);
    }

    public void Clear() {
      ((ICollection<KeyValuePair<TKey, IInventoryItem>>)_items).Clear();
    }

    public bool Contains(KeyValuePair<TKey, IInventoryItem> item) {
      return ((ICollection<KeyValuePair<TKey, IInventoryItem>>)_items).Contains(item);
    }

    public bool ContainsKey(TKey key) {
      return ((IDictionary<TKey, IInventoryItem>)_items).ContainsKey(key);
    }

    public void CopyTo(KeyValuePair<TKey, IInventoryItem>[] array, int arrayIndex) {
      ((ICollection<KeyValuePair<TKey, IInventoryItem>>)_items).CopyTo(array, arrayIndex);
    }

    public bool Remove(TKey key) {
      return ((IDictionary<TKey, IInventoryItem>)_items).Remove(key);
    }

    public bool Remove(KeyValuePair<TKey, IInventoryItem> item) {
      return ((ICollection<KeyValuePair<TKey, IInventoryItem>>)_items).Remove(item);
    }

    public bool TryGetValue(TKey key, out IInventoryItem value) {
      return ((IDictionary<TKey, IInventoryItem>)_items).TryGetValue(key, out value);
    }

    IEnumerator<KeyValuePair<TKey, IInventoryItem>> IEnumerable<KeyValuePair<TKey, IInventoryItem>>.GetEnumerator() {
      return ((IEnumerable<KeyValuePair<TKey, IInventoryItem>>)_items).GetEnumerator();
    }

    #endregion

  }
}
