﻿using Meep.Tech.Data;
using Meep.Tech.Data.Attributes.Models;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Games.Items.Inventories.Basic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.Games.Items.Inventories {

  /// <summary>
  /// The base of most types of Inventory for containing Items.
  /// </summary>
  [DataModel("Inventory")]
  public abstract partial class Inventory 
    : IModel<
        Inventory,
        Inventory.Type,
        Inventory.Type.Id
      >,
      IInventory,
      IUnique,
      IOwned 
  {

    /// <summary>            
    /// Value for empty inventory or grid slots.           
    /// </summary>
    public const int EmptyStackValue = -1;

    #region Model Data

    /// <summary>
    /// Base class for Inventory params
    /// </summary>
    public class Params : Param {

      /// <summary>
      /// The custom name of this inventory
      /// </summary>
      public new static Params CustomName {
        get;
      } = new Params("Name", typeof(string));

      protected Params(string name, System.Type type, string externalIdEndingPrefix = null) 
        : base(name, type, $"Inventories.{externalIdEndingPrefix ?? ""}") {}
    }

    /// <summary>
    /// The id of the profile, should be UUID
    /// </summary>
    [ModelIdField]
    string IUnique.id {
      get => _id;
      set => _id = value;
    } string _id;

    /// <summary>
    /// The user set name of the profile
    /// </summary>
    [ModelDataField]
    public string name {
      get;
      private set;
    }

    /// <summary>
    /// Owner of the inventory
    /// </summary>
    [ModelOwnerField]
    string IOwned.owner {
      get => _owner;
      set => _owner = value;
    } string _owner;

    /// <summary>
    /// The (potential) limit on how many items this can hold
    /// </summary>
    public virtual int? maxQuantity {
      get;
      protected set;
    }

    /// <summary>
    /// As enumerable for the contents
    /// </summary>
    public IEnumerable<IInventoryItem> contents
      => this;

    /// <summary>
    /// all of the items, including nulls and none types.
    /// </summary>
    protected abstract IEnumerable<(IInventoryItem item, Inventory.Slot location)> slots {
      get;
    }

    #endregion

    /// <summary>
    /// If the inventory can't accept new items
    /// </summary>
    public virtual bool hasMaxQuantity
      => maxQuantity.HasValue;

    /// <summary>
    /// If the inventory can't accept new items
    /// </summary>
    public virtual bool isFull
      => maxQuantity.HasValue && quantity >= maxQuantity;

    /// <summary>
    /// The total quantity of items:
    /// </summary>
    public virtual int quantity
      => (this as IEnumerable<IInventoryItem>)
        .Sum(item => item is IInventoryMultipleItemContainer container 
          ? container.quantity 
          : 1
        );

    /// <summary>
    /// The type:
    /// </summary>
    public Type type {
      get;
    } Type IModel<Inventory, Type, Type.Id>.type 
      => type;

    #region Initialization

    /// <summary>
    /// Make a new inventory:
    /// </summary>
    protected Inventory(Type archetype, string ownerId = null, string uniqueID = null) { 
      type = archetype;
      _owner = ownerId;
      _id = uniqueID;
    }

    public Inventory clone(bool newUniqueId = true, string newUniqueIdToken = null)
      => _duplicate(newUniqueId, newUniqueIdToken);

    /// <summary>
    /// Helper to just group a bunch of items into a simple inventory.
    /// </summary>
    public static SimpleIdBasedInventory Make(params IInventoryItem[] items)
      => new SimpleIdBasedInventory(items);

    #region Required Abstract Functionality.

    /// <summary>
    /// Make a copy of this inventory and all the items in it:
    /// </summary>
    protected abstract Inventory _duplicate(bool newUniqueId = true, string newUniqueIdToken = null);

    #endregion

    #endregion

    #region Item Accessors

    public IInventoryItem this[Slot slot] 
      => get(slot);

    public virtual IInventoryItem get(Slot slot) 
      => tryToGet(slot, out IInventoryItem item) 
        ? item 
        : throw new IndexOutOfRangeException($"No valid item at slot: {slot}, in inventory {this}");

    public virtual IInventoryItem tryToGet(Slot slot) 
      => tryToGet(slot, out IInventoryItem item) ? item : null;

    public virtual bool tryToGet(Slot slot, out IInventoryItem item)
      => contains(slot, out item);

    public virtual bool contains(IInventoryItem item)
      => (this as IEnumerable<IInventoryItem>).SelectMany(item => item is Item.IBox box ? box : null).Contains(item);

    public bool contains(Slot slot, out IInventoryItem item)
      => (item = null) == null && _isValidSlot(slot) && _contains(slot, out item) && item != null;

    public bool contains(Slot slot)
      =>  _isValidSlot(slot) && _contains(slot, out IInventoryItem item) && item != null;

    #region Required Abstract Functionality.

    /// <summary>
    /// Logic for getting the item contained at the slot.
    /// _isValidSlot and null return checks are already handled by the parent function call.
    /// </summary>
    public abstract bool _contains(Slot slot, out IInventoryItem item);

    #endregion

    #region Add

    #region Basic Functionality

    #region Required Abstract Functionality.

    /// <summary>
    /// How this inventory adds an item without a provided slot.
    /// This is called after the validation functions, and only recieves successfullyAddedItems from validation as the item input.
    /// </summary>
    protected abstract bool _tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Inventory.Slot> modifiedSlots);

    /// <summary>
    /// How this inventory adds an item to a specific slot
    /// This is called after the validation functions, and only recieves successfullyAddedItems from validation as the item input.
    /// _isSlotValid() as well as a contains() check are done beforehand to make sure the slot is valid and empty, or can fit the contents of item.
    /// <param name="slotIsOccupied">If validation determined there's already an item in this slot, but you should be able to fit more items there.</param>
    /// </summary>
    protected abstract bool _tryToAdd(IInventoryItem item, Inventory.Slot slot, bool slotIsOccupied, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem);

    #endregion

    #region Validation

    /// <summary>
    /// Optional validator for slots
    /// </summary>
    protected virtual bool _isValidSlot(Inventory.Slot slot)
      => true;

    /// <summary>
    /// Basic validation for nulls and quantity before adding new items.
    /// you may need to override this if you want to accept nulls.
    /// </summary>
    protected virtual bool _validateBeforeTryingToAdd(IInventoryItem item, out IInventoryItem willBeLeftover, out IInventoryItem willBeSuccessfullyAdded) {
      if (item == null) {
        willBeLeftover = null;
        willBeSuccessfullyAdded = null;
        return true;
      }

      if (maxQuantity.HasValue && quantity >= maxQuantity) {
        willBeLeftover = item;
        willBeSuccessfullyAdded = null;
        return false;
      }

      if (maxQuantity.HasValue
        && item is IInventoryMultipleItemContainer container
        && quantity + container.quantity > maxQuantity
      ) {
        willBeSuccessfullyAdded = container.split(maxQuantity.Value - quantity);
        willBeLeftover = container;
        return false;
      } else {
        willBeSuccessfullyAdded = item;
        willBeLeftover = null;
        return true;
      }
    }

    /// <summary>
    /// Basic validation for nulls, slot space, quantity before adding new items.
    /// you may need to override this if you want to accept nulls.
    /// </summary>
    protected virtual bool _validateBeforeTryingToAddToSlot(Inventory.Slot slot, IInventoryItem item, out IInventoryItem willBeLeftover, out IInventoryItem willBeSuccessfullyAdded, out bool slotIsOccupied) {
      slotIsOccupied = false;
      if ( _validateBeforeTryingToAdd(item, out willBeLeftover, out willBeSuccessfullyAdded)
        // make sure the slot itself is valid
        && _isValidSlot(slot)
        // if we already have an item in the slot, and it's not the same as the current one it can't be added: 
        && (!contains(slot, out IInventoryItem existing) || existing.containsTheSameItemsAs(item))
      ) {
        // if the container in this spot exists:
        if (existing != null) {
          slotIsOccupied = true;
          if (existing is IInventoryMultipleItemContainer container) {
            // if there's a full container in this spot, return false
            if (container.isFull) {
              return false;
            }
            // if there's not enough space in the current container to hold the whole of the item:
            if (container.remainingSpace < item.getSlotQuantity()) {
              // trim what we can't fit
              willBeSuccessfullyAdded = (item as IInventoryMultipleItemContainer).split(container.remainingSpace);
              willBeLeftover = item;
            }
          }
        }
        return true;
      }

      return false;
    }

    #endregion

    public bool tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Inventory.Slot> modifiedSlots) {
      bool allAddedSucccessfully = true;
      List<Slot> modifiedSlotList = new List<Slot>();

      if (_validateBeforeTryingToAdd(item, out leftover, out successfullyAddedItem)) {
        if (_tryToAdd(successfullyAddedItem, out IInventoryItem moreLeftovers, out successfullyAddedItem, out var newModifiedSlots)) {
          if (moreLeftovers != null) {
            leftover = leftover == null
              ? moreLeftovers
              : moreLeftovers.combine(leftover);
          }
        } else {
          allAddedSucccessfully = false;
        }
        if (newModifiedSlots != null) {
          modifiedSlotList.AddRange(newModifiedSlots);
        }
      } else {
        allAddedSucccessfully = false;
      }

      if (successfullyAddedItem != null) {
        _onItemAdded(successfullyAddedItem, modifiedSlotList);
      }

      modifiedSlots = modifiedSlotList;
      return allAddedSucccessfully;
    }

    public bool tryToAdd(Inventory.Slot inventorySlot, IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem) {
      bool allAddedSucccessfully = true;

      if (_validateBeforeTryingToAddToSlot(inventorySlot, item, out leftover, out successfullyAddedItem, out bool slotIsOccupied)) {
        if (_tryToAdd(successfullyAddedItem, inventorySlot, slotIsOccupied, out IInventoryItem moreLeftovers, out successfullyAddedItem)) {
          if (moreLeftovers != null) {
            leftover = leftover == null
              ? moreLeftovers
              : moreLeftovers.combine(leftover);
          }
        } else {
          allAddedSucccessfully = true;
        }
      }

      if (successfullyAddedItem != null) {
        _onItemAdded(successfullyAddedItem, inventorySlot.AsEnumerable());
      }

      return allAddedSucccessfully;
    }

    public bool tryToAdd(IEnumerable<IInventoryItem> items, out IEnumerable<IInventoryItem> leftovers, out IEnumerable<IInventoryItem> successfullyAddedItems, out IEnumerable<Inventory.Slot> modifiedSlots) {
      bool allAddedSuccessfully = true;
      List<IInventoryItem> leftoverItems = new List<IInventoryItem>();
      List<IInventoryItem> successfullyAddedItemList = new List<IInventoryItem>();
      List<Inventory.Slot> modifiedSlotList = new List<Inventory.Slot>();
      leftovers = leftoverItems;

      foreach (IInventoryItem item in items) {
        if (!tryToAdd(item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out var newModifiedSots)) {
          leftoverItems.Add(leftover);
          allAddedSuccessfully = false;
        }
        if (successfullyAddedItem != null) {
          successfullyAddedItemList.Add(successfullyAddedItem);
        }
        if (newModifiedSots != null) {
          modifiedSlotList.AddRange(newModifiedSots);
        }
      }

      successfullyAddedItems = successfullyAddedItemList;
      modifiedSlots = modifiedSlotList;
      return allAddedSuccessfully;
    }

    public bool tryToAdd(out IEnumerable<IInventoryItem> leftovers, params IInventoryItem[] items)
      => tryToAdd(items, out leftovers, out _);

    /// <summary>
    /// Just sets the item's ownership
    /// </summary>
    /// <param name="item"></param>
    protected virtual void _afterItemAdded(IInventoryItem item, IEnumerable<Slot> addedTo) { }

    /// <summary>
    /// Just sets the item's ownership
    /// </summary>
    /// <param name="item"></param>
    void _onItemAdded(IInventoryItem item, IEnumerable<Slot> addedTo) {
      item.setContainingInventoryId(this.getUniqueId());
      _afterItemAdded(item, addedTo);
    }

    #endregion

    /// <summary>
    /// Used for the ctor
    /// </summary>
    public void Add(IInventoryItem item)
      => tryToAdd(item);

    public bool tryToAdd(IInventoryItem item)
      => tryToAdd(item, out _, out _);

    public bool tryToAddAll(IEnumerable<IInventoryItem> items)
      => tryToAdd(items, out _, out _);

    public bool tryToAdd(IInventoryItem item, out IInventoryItem leftover)
      => tryToAdd(item, out leftover, out _);

    public bool tryToAdd(IEnumerable<IInventoryItem> items, out IEnumerable<IInventoryItem> leftovers)
      => tryToAdd(items, out leftovers, out _);

    public bool tryToAdd(Item item)
      => tryToAdd(out _, item);

    public bool tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem)
      => tryToAdd(item, out leftover, out successfullyAddedItem, out _);

    public bool tryToAdd(IEnumerable<IInventoryItem> items, out IEnumerable<IInventoryItem> leftovers, out IEnumerable<IInventoryItem> successfullyAddedItems)
      => tryToAdd(items, out leftovers, out successfullyAddedItems, out _);

    public bool tryToAdd(Item item, Inventory.Slot inventorySlot)
      => tryToAdd(inventorySlot, item, out _, out _);

    public bool tryToAdd(IInventoryItem item, Inventory.Slot inventorySlot)
      => tryToAdd(inventorySlot, item, out _, out _);

    public bool tryToAdd(Item.Stack item, out Item.Stack leftovers)
      => tryToAdd(item, out leftovers);

    public bool tryToAdd(Item.Stack item, Inventory.Slot inventorySlot, out Item.Stack leftovers)
      => tryToAdd(item, inventorySlot, out leftovers);

    public bool tryToAdd(Item.IBox item, out Item.IBox leftovers)
      => tryToAdd(item, out leftovers);

    public bool tryToAdd(Item.IBox item, Inventory.Slot inventorySlot, out Item.IBox leftovers)
      => tryToAdd(item, inventorySlot, out leftovers);

    public bool tryToAdd<TContainer>(TContainer item, out TContainer leftovers)
      where TContainer : IInventoryMultipleItemContainer 
    {
      bool allAdded = tryToAdd(out IEnumerable<IInventoryItem> leftoverStack, item);
      if (!allAdded) {
        leftovers = (TContainer)leftoverStack.First();
      } else {
        leftovers = default;
      }

      return allAdded;
    }

    public bool tryToAdd<TContainer>(
      TContainer item,
      Inventory.Slot inventorySlot,
      out TContainer leftovers
    ) where TContainer : IInventoryMultipleItemContainer {
      bool allAdded = tryToAdd(inventorySlot, item, out var leftover, out _);
      if (!allAdded) {
        leftovers = (TContainer)leftover;
      } else {
        leftovers = default;
      }

      return allAdded;
    }

    #endregion

    #region Swap
     
    public virtual bool tryToSwap(Inventory.Slot inventorySlot, IInventoryItem item, out IInventoryItem oldItem) {
      oldItem = removeAt(inventorySlot);
      return tryToAdd(item, inventorySlot);
    }

    #endregion

    #region Remove

    #region Required Abstract Functionality.

    /// <summary>
    /// Logic for remove-at.
    /// quantity = 0 is covered in the base class.
    /// </summary>
    protected abstract IInventoryItem _removeAt(Inventory.Slot slot, int quantity = int.MaxValue);

    public abstract IEnumerable<IInventoryItem> empty();

    #endregion

    public virtual IEnumerable<IInventoryItem> removeEachUntil(
      Func<IInventoryItem, (bool remove, bool @continue)> removeUntil = null,
      int quantity = int.MaxValue
    ) {
      if (quantity == 0) {
        return null;
      }

      int quantityToGrab = quantity;
      List<IInventoryItem> removed = new List<IInventoryItem>();

      // loop though each item and slot and remove it if it meets the requirements.
      foreach ((IInventoryItem item, Inventory.Slot slot) in slots) {
        bool remove, @continue;
        (remove, @continue) = removeUntil(item);
        if (remove) {
          if (item is IInventoryMultipleItemContainer container && quantityToGrab - container.getSlotQuantity() < 0) {
            // if we only removed part of a container, don't remove it from the inventory.
            removed.Add(container.split(quantityToGrab));
            quantityToGrab = 0;
            @continue = false;
          } else {
            removed.Add(_removeAt(slot));
            quantityToGrab -= item.getSlotQuantity();
          }
        }

        if (!@continue) {
          break;
        }
      }

      return removed;
    }

    public virtual IEnumerable<IInventoryItem> removeEach(
      Func<IInventoryItem, bool> shouldRemove = null,
      int quantity = int.MaxValue
    ) => removeEachUntil(item => (shouldRemove(item), true), quantity);

    public virtual IInventoryItem removeAt(Inventory.Slot slot, int quantity = int.MaxValue)
      => quantity > 0 && _isValidSlot(slot) ? _removeAt(slot, quantity) : null;

    public virtual IInventoryItem removeFirst(IInventoryItem item, int quantity = int.MaxValue)
      => removeEachUntil(currentItem => {
        bool found = item.Equals(currentItem);
        return (found, !found);
      }, quantity).First();

    public virtual IEnumerable<IInventoryItem> removeAll(IInventoryItem item, int quantity = int.MaxValue)
      => quantity == 0 ? Enumerable.Empty<IInventoryItem>() : removeEachUntil(currentItem => (item.Equals(currentItem), true), quantity);

    public virtual bool tryToEmptyInto(IInventory otherInventory) {
      if (quantity == 0) {
        return true;
      }

      removeEachUntil(item => {
        bool success = otherInventory.tryToAdd(out _, item);
        return (success, success);
      });

      return !this.Any();
    }

    #endregion

    #endregion

    #region IEnumerable

    public virtual IEnumerator<IInventoryItem> _getEnumerator()
      => slots.Select(slot => slot.item).GetEnumerator();

    public IEnumerator<IInventoryItem> GetEnumerator()
      => _getEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
      => GetEnumerator();

    #endregion

  }
}
