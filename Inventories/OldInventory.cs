﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Serialization;

namespace Meep.Tech.Games.Items.Inventories {
  /*
  /// <summary>
  /// A basic inventory
  /// </summary>
  [DataModel("Inventory")]
  public abstract partial class Inventory 
    : Model<
      Inventory, 
      Inventory.Type,
      Inventory.Type.Id
    >,
      IInventory,
      IUnique,
      IOwned 
  {

    /// <summary>
    /// Value for empty grid slots
    /// </summary>
    public const int EmptyStackValue = -1;

    #region Model Data

    /// <summary>
    /// The id of the profile, should be UUID
    /// </summary>
    [field:ModelIdField]
    string IUnique.id {
      get;
      set;
    }

    /// <summary>
    /// The user set name of the profile
    /// </summary>
    [ModelDataField]
    public string name {
      get;
      private set;
    }

    /// <summary>
    /// Owner of the inventory
    /// </summary>
    [field:ModelOwnerField]
    string IOwned.owner {
      get;
      set;
    }

    #endregion

    /// <summary>
    /// base for making a new inventory
    /// </summary>
    /// <param name="ownerId"></param>
    /// <param name="inventoryType"></param>
    /// <param name="name"></param>
    protected Inventory(Inventory.Type inventoryType) 
      : base(inventoryType) {}

    /// <summary>
    /// Just sets the item's ownership
    /// </summary>
    /// <param name="item"></param>
    protected void onItemAdded(IInventoryItem item) {
      item.setContainingInventoryId(this.getUniqueId());
    }

    #region Implimented IInventory

    /// <summary>
    /// Add multuple items
    /// </summary>
    /// <returns>leftovers that don't fit</returns>
    public IEnumerable<Item> tryToAdd(IEnumerable<Item> items, out IEnumerable<Item> successfullyAddedItems) {
      List<Item> successfullyAddedItemList = new List<Item>();
      List<Item> leftovers = new List<Item>();
      foreach (Item item in items) {
        leftovers.Add(tryToAdd(item, out Item successfullyAddedItemStack));
        if (successfullyAddedItemStack != null) {
          successfullyAddedItemList.Add(successfullyAddedItemStack);
        }
      }

      successfullyAddedItems = successfullyAddedItemList.ToArray();
      return leftovers.ToArray();
    }

    public IEnumerator<Item> GetEnumerator() {
      return items.Where(item => item != null).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator() {
      return GetEnumerator();
    }

    #endregion

    #region Abstract IInventory

    protected abstract IEnumerable<Item> items {
      get;
    }

    /// <summary>
    /// all of the items, including nulls and none types.
    /// </summary>
    [ItemSlotsDataField]
    protected abstract IEnumerable<(Item item, InventorySlot location)> slots {
      get;
    }

    /// <summary>
    /// This forces the child class to impliment _clone
    /// </summary>
    public override Inventory clone(bool newUniqueId = true, string newUniqueIdToken = null) {
      return _clone(newUniqueId, newUniqueIdToken);
    }

    /// <summary>
    /// This must be implimented to clone:
    /// </summary>
    /// <returns></returns>
    protected abstract Inventory _clone(bool newUniqueId = true, string newUniqueIdToken = null);
    public abstract IEnumerable<Item> empty();
    public abstract IEnumerable<Item> emptyInto(IInventory otherInventory, out IEnumerable<Item> successfullyAddedItems);
    public abstract IEnumerable<Item> remove(Item item, int? quantity = null);
    public abstract Item tryToAdd(Item item, out Item successfullyAddedItem);
    public abstract Item tryToAdd(Item item, InventorySlot inventorySlot, out Item successfullyAddedItem);

    #endregion

    #region Serialization

    /// <summary>
    /// Field conversion attribute for items in an inventory
    /// </summary>
    public class ItemSlotsDataFieldAttribute : ChildModelsCollectionFieldAttribute {
      public ItemSlotsDataFieldAttribute() : base("items", typeof(Item), DeleteAllOldChildrenBeforeSavingNewChildrenOfThisType: true) { }

      /// <summary>
      /// Save the items to the DB and save the light items + slot data to a list in this doc
      /// </summary>
      public override Func<object, Serializer.SerializedDataCollection> ChildModelSerializationFunction
        => slots => {
          Serializer.SerializedDataCollection serializedItems 
            = new Serializer.SerializedDataCollection();
          string itemTableName = null;
          foreach ((Item item, InventorySlot slotLocation) in slots as IEnumerable<(Item, InventorySlot)>) {
            /// serialize the item's basic doc:
            IUnique itemModel = item;
            itemTableName ??= itemModel.GetTableName();
            Serializer.SerializedDataCollection itemData = itemModel.serialize();

            // build the item location if it matters
            JObject locationData = new JObject();
            if (slotLocation.gridLocation != (EmptyStackValue, EmptyStackValue)) {
              locationData.Add("girdLocation", JObject.FromObject(new Dictionary<string, object> {
                { "x", slotLocation.gridLocation.x },
                { "y",  slotLocation.gridLocation.y }
              }));
            }
            if (slotLocation.stackId != EmptyStackValue) {
              locationData.Add("stackId", slotLocation.stackId);
            }
            if (locationData.Count > 0) {
              itemData.appendJsonFieldToFirstSerializedDataItemFor(
                itemModel.id,
                itemTableName,
                "location",
                locationData
              );
            }

            serializedItems.Merge(itemData);
          }

          return serializedItems;
        };

      /// <summary>
      /// Unload location data and add the item to inventory:
      /// </summary>
      public override Func<string, string, Serializer.SerializedData, IUnique, ISerializeable> ChildModelAdvancedDeserializationFunction
        => (itemId, itemType, serializedModel, inventory) => {
          Item baseItem =  Item.Types.Get(itemType).Make(serializedModel.sqlData, serializedModel.jsonData);
          // deserialize location data for each slot if it exists:
          if (serializedModel.jsonData.TryGetValue("location", out JToken locationData)) {
            int? stackId = null;
            Meep.Tech.Games.Basic.Geometry.Coordinate? gridLocation = null;
            if ((locationData as JObject).TryGetValue("gridLocation", out JToken gridLocationData)) {
              gridLocation = (gridLocationData.Value<int>("x"), gridLocationData.Value<int>("y"));
            }
            if ((locationData as JObject).TryGetValue("stackId", out JToken stackIdData)) {
              stackId = stackIdData.Value<int>();
            }
            InventorySlot inventorySlot = new InventorySlot(gridLocation, stackId);
            (inventory as Inventory).tryToAdd(baseItem, inventorySlot, out _);
          } // If there's no slot data just toss it into the inventory:
          else {
            (inventory as Inventory).tryToAdd(baseItem, out _);
          }

          return baseItem;
        };
    }

    #endregion
  }*/
}
  