﻿using System;
using System.Collections.Generic;

namespace Meep.Tech.Games.Items.Inventories {

  /// <summary>
  /// a robust collection of items, stacks, and boxes
  /// </summary>
  public interface IInventory : IEnumerable<IInventoryItem> {

    /// <summary>
    /// If the inventory can't accept new items
    /// </summary>
    bool isFull {
      get;
    }

    /// <summary>
    /// The quantity of items and stack totals.
    /// </summary>
    int quantity {
      get;
    }

    /// <summary>
    /// All the contents for enumrations
    /// </summary>
    IEnumerable<IInventoryItem> contents {
      get;
    }

    /// <summary>
    /// Get the item at the given slot if it exists.
    /// </summary>
    /// <exception cref="IndexOutOfRangeException"?>If the item isn't found at the slot, or the slot is out of bounds</exception>
    IInventoryItem this[Inventory.Slot slot] {
      get;
    }

    /// <summary>
    /// Get the item at the given slot if it exists.
    /// </summary>
    /// <exception cref="IndexOutOfRangeException"?>If the item isn't found at the slot, or the slot is out of bounds</exception>
    IInventoryItem get(Inventory.Slot slot);

    /// <summary>
    /// Get the item at the given slot if it exists, or return null if it doesnt
    /// </summary>
    IInventoryItem tryToGet(Inventory.Slot slot);

    /// <summary>
    /// Get the item at the given slot if it exists
    /// </summary>
    /// <returns>true if exists</returns>
   bool tryToGet(Inventory.Slot slot, out IInventoryItem item);

    /// <summary>
    /// If this inventory contains the given item:
    /// </summary>
    bool contains(IInventoryItem item);

    /// <summary>
    /// If this inventory contains an item at the given slot.
    /// outs the found item if there was one.
    /// </summary>
    bool contains(Inventory.Slot slot, out IInventoryItem item);

    #region Add Single

    #region Basic Functionality

    /// <summary>
    /// Add an item to this inventory arbitrarily, without a specific slot provided.
    /// </summary>
    /// <param name="item">The item we want to try to add</param>
    /// <param name="leftover">The item, or stack of items we were not able to add</param>
    /// <param name="successfullyAddedItem">The successfully added item or stack of items</param>
    /// <returns>true if there were no leftovers and the full item or stack was added successfully</returns>
    bool tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem, out IEnumerable<Inventory.Slot> modifiedSlots);

    /// <summary>
    /// Try to add an item to the inventory at a specific slot.
    /// </summary>
    /// <param name="inventorySlot">The slot we want to put the item into</param>
    /// <param name="item">The item we want to try to add</param>
    /// <param name="leftover">The item, or stack of items we were not able to add</param>
    /// <param name="successfullyAddedItem">The successfully added item or stack of items</param>
    /// <returns>true if there were no leftovers and the full item or stack was added successfully</returns>
    bool tryToAdd(Inventory.Slot inventorySlot, IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem);

    #endregion

    /// <summary>
    /// Add an item to the inventory
    /// </summary>
    bool tryToAdd(IInventoryItem item);

    /// <summary>
    /// Add an item to the inventory at the given slot
    /// </summary>
    bool tryToAdd(IInventoryItem item, Inventory.Slot inventorySlot);

    /// <summary>
    /// Add an item to this inventory arbitrarily, without a specific slot provided.
    /// </summary>
    bool tryToAdd(IInventoryItem item, out IInventoryItem leftover);

    /// <summary>
    /// Add an item to this inventory arbitrarily, without a specific slot provided.
    /// </summary>
    bool tryToAdd(IInventoryItem item, out IInventoryItem leftover, out IInventoryItem successfullyAddedItem);

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    bool tryToAdd(Item item);

    /// <summary>
    /// Try to add an item to a specific inventory slot
    /// </summary>
    bool tryToAdd(Item item, Inventory.Slot inventorySlot);

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    bool tryToAdd(Item.Stack item, out Item.Stack leftovers);

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    bool tryToAdd(Item.Stack item, Inventory.Slot inventorySlot, out Item.Stack leftovers);

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    bool tryToAdd(Item.IBox item, out Item.IBox leftovers);

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    bool tryToAdd(Item.IBox item, Inventory.Slot inventorySlot, out Item.IBox leftovers);

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    bool tryToAdd<TContainer>(TContainer item, out TContainer leftovers)
      where TContainer : IInventoryMultipleItemContainer;

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    public bool tryToAdd<TContainer>(
      TContainer item,
      Inventory.Slot inventorySlot,
      out TContainer leftovers
    ) where TContainer : IInventoryMultipleItemContainer;

    #endregion

    #region Add Multiple

    #region Basic Functionality

    /// <summary>
    /// Add items to the inventory
    /// </summary>
    bool tryToAdd(out IEnumerable<IInventoryItem> leftovers, params IInventoryItem[] items);

    #endregion

    /// <summary>
    /// Add items to this inventory arbitrarily, without a specific slot provided.
    /// </summary>
    bool tryToAdd(IEnumerable<IInventoryItem> items, out IEnumerable<IInventoryItem> leftovers);

    /// <summary>
    /// Add items to this inventory arbitrarily, without a specific slot provided.
    /// </summary>
    bool tryToAddAll(IEnumerable<IInventoryItem> items);

    #endregion

    #region Swap

    /// <summary>
    /// Add item to the inventory at a specific slot, replacing the item already there.
    /// This is helpfull because the gapless tryToAdd may just insert inbetween.
    /// </summary>
    bool tryToSwap(Inventory.Slot inventorySlot, IInventoryItem item, out IInventoryItem oldItem);

    #endregion

    #region Remove

    /// <summary>
    /// Removes each item until that's true until contine isn't true or we reach the max quantity
    /// </summary>
    IEnumerable<IInventoryItem> removeEachUntil(
      Func<IInventoryItem, (bool remove, bool @continue)> removeUntil = null,
      int quantity = int.MaxValue
    );

    /// <summary>
    /// Removes each item that returns true from the given shouldRemove function.
    /// </summary>
    IEnumerable<IInventoryItem> removeEach(
      Func<IInventoryItem, bool> shouldRemove = null,
      int quantity = int.MaxValue
    );

    /// <summary>
    /// remove items from the inventory that match the item
    /// </summary>
    /// <returns>removed items</returns>
    IEnumerable<IInventoryItem> removeAll(IInventoryItem item, int quantity = int.MaxValue);

    /// <summary>
    /// remove items from the inventory that match the item
    /// </summary>
    /// <returns>removed items</returns>
    IInventoryItem removeFirst(IInventoryItem item, int quantity = int.MaxValue);

    /// <summary>
    /// remove the item from the inventory at the given slot
    /// </summary>
    IInventoryItem removeAt(Inventory.Slot slot, int quantity = int.MaxValue);

    /// <summary>
    /// empty all the items from the inventory
    /// </summary>
    IEnumerable<IInventoryItem> empty();

    /// <summary>
    /// Empty this inventory into another one
    /// </summary>
    /// <returns>Success. Leftovers that didn't fit stay in the original inventory</returns>
    bool tryToEmptyInto(IInventory otherInventory);

    #endregion
  }
}
