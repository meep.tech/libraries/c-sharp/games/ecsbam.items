﻿using System.Collections.Generic;

namespace Meep.Tech.Games.Items.Inventories {

  /// <summary>
  /// An inventory Item holder, or a stack of items that can be held in an inventory
  /// </summary>
  public interface IInventoryMultipleItemContainer
    : IInventoryItem,
      IEnumerable<IInventoryItem> 
  {

    bool isFull {
      get;
    }

    /// <summary>
    /// How many of this item are in this stack
    /// </summary>
    int quantity {
      get;
      internal protected set;
    }

    /// <summary>
    /// How many of this item are in this stack
    /// </summary>
    int maxQuantity {
      get;
    }

    /// <summary>
    /// How many of this item are in this stack
    /// </summary>
    int remainingSpace {
      get;
    }

    /// <summary>
    /// try to take the contents from a stack or box of items into this one
    /// </summary>
    bool tryToTakeContents(IInventoryMultipleItemContainer other, out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems);

    /// <summary>
    /// try to add more items to this container as full IInventoryItems
    /// </summary>
    bool tryToAdd(IEnumerable<IInventoryItem> items, out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems);

    /// <summary>
    /// try to add more items to this container as full IInventoryItems
    /// </summary>
    bool tryToAdd(out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems, params IInventoryItem[] items);

    /// <summary>
    /// Try to add a sinlge item, or take the contents of a collection and add them to this item container.
    /// </summary>
    bool tryToAddOrTakeFrom(IInventoryItem other, out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems)
      => other is IInventoryMultipleItemContainer otherContainer
        ? tryToTakeContents(otherContainer, out leftoverQuantity, out leftoverItems, out successfullyAddedItems)
        : tryToAdd(out leftoverQuantity, out leftoverItems, out successfullyAddedItems, other);

    /// <summary>
    /// Split a container in two, giving the second one containing up to the requested result size.
    /// Result size may be smaller if there weren't enough items.
    /// </summary>
    IInventoryMultipleItemContainer split(int resultQuantity);

    /// <summary>
    /// Used internally to forcefully add items to and expand a container.
    /// </summary>
    internal IInventoryMultipleItemContainer addAndExpandWith(IInventoryItem inventoryItem);
  }
}