﻿using Meep.Tech.Data.Base;

namespace Meep.Tech.Games.Items.Inventories {
  public partial class Inventory {

    /// <summary>
    /// All inventory types
    /// </summary>
    public static Archetype.Collection<Inventory, Type, Type.Id> Types {
      get;
    } = new Archetype.Collection<Inventory, Type, Type.Id>();

    /// <summary>
    /// Base type for Inventory types.
    /// Inventory types are archetypes of inventories, like PlayerBackpack, capable of generating a savable inventory model.
    /// </summary>
    public abstract partial class Type : Archetype<Inventory, Type, Type.Id> {

      /// <summary>
      /// Represents no inventory
      /// </summary>
      public sealed class None : Type {
        None() : base(Id.None) { }

        // TODO: does ecsbam need to have a handler for null models from initialize?
        protected override Inventory InitializeModel(Model.Params @params = null)
          => null;

        protected override Inventory ConfigureModel(Inventory model, Model.Params @params = null) {
          if (model != null) {
            model.name = @params.GetAs(Inventory.Params.CustomName, Name);
          }

          return base.ConfigureModel(model, @params);
        }
      }

      protected Type(Id archetypeId)
        : base(archetypeId, Types) { }
    }
  }
}
