﻿using Meep.Tech.Games.Basic.Geometry;
using System;

namespace Meep.Tech.Games.Items.Inventories {

  public partial class Inventory {

    /// <summary>
    /// Struct for serializing an item's current location in storage
    /// </summary>
    public struct Slot : IEquatable<Slot> {

      /// <summary>
      /// The inventory containing this item
      /// Null means it's overworld/chunk based by default.
      /// </summary>
      public Type containingInventoryType {
        get;
      }

      /// <summary>
      /// The pivot/location the item is placed in the inventory
      /// (Can also be used to store the chunk ID if it's a chunk storing the item)
      /// </summary>
      public Coordinate gridLocation {
        get;
      }

      /// <summary>
      /// The stack id of the item in the given inventory
      /// Used for drops. and other things
      /// </summary>
      public object stackKey {
        get;
      }

      /// <summary>
      /// If this inventory has a parent location, like a chest in a chunk, this will be the id of the chest
      /// If it's between players, this is the player id.
      /// </summary>
      public object parentKey {
        get;
      }

      /// <summary>
      /// extra key
      /// </summary>
      public object otherKey {
        get;
      }

      /// <summary>
      /// The bar slot index shorcut for bar/slot based inventories
      /// </summary>
      public int barSlotIndex
        => gridLocation.x;

      /// <summary>
      ///  Make a slot with just a generic item key
      /// </summary>
      public Slot(Type containingInventory, object key, string parentInventoryUniqueId = null) {
        gridLocation = null;
        containingInventoryType = containingInventory ?? Types.Get<Type.None>();
        parentKey = parentInventoryUniqueId ?? (object)EmptyStackValue;
        stackKey = key;
        otherKey = null;
      }

      /// <summary>
      /// Make a player item slot for just local use.
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      public Slot(Type containingInventory, Coordinate pivot, string parentInventoryUniqueId = null, object stackKey = null) {
        gridLocation = pivot;
        this.containingInventoryType = containingInventory ?? Types.Get<Type.None>();
        parentKey = parentInventoryUniqueId ?? (object)EmptyStackValue;
        this.stackKey = stackKey ?? EmptyStackValue;
        otherKey = null;
      }

      /// <summary>
      /// Make a player item slot for just local use.
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      public Slot(Type containingInventory, int barSlotId, string parentInventoryUniqueId = null) {
        gridLocation = (barSlotId, 0);
        containingInventoryType = containingInventory ?? Types.Get<Type.None>();
        parentKey = parentInventoryUniqueId ?? (object)EmptyStackValue;
        stackKey = barSlotId;
        otherKey = null;
      }

      /// <summary>
      /// Make a slot for an item drop in a chunk.
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      public Slot(string itemStackId, Coordinate chunkAxialKey, Type inventoryType = null) {
        containingInventoryType = inventoryType ?? Types.Get<Type.None>();
        stackKey = itemStackId;
        gridLocation = chunkAxialKey;
        parentKey = EmptyStackValue;
        otherKey = null;
      }

      /// <summary>
      /// Make a player item slot for just local use.
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      public Slot(string itemStackId, Type containingInventory, object otherKey = null) {
        gridLocation = default;
        containingInventoryType = containingInventory ?? Types.Get<Type.None>();
        parentKey = EmptyStackValue;
        stackKey = itemStackId;
        this.otherKey = otherKey;
      }

      /// <summary>
      /// Make a player item slot for just local use.
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      public Slot(Coordinate pivot) {
        gridLocation = pivot;
        containingInventoryType = Types.Get<Type.None>();
        parentKey = EmptyStackValue;
        stackKey = EmptyStackValue;
        otherKey = null;
      }

      /// <summary>
      /// Make a slot with an id instead of a pivot
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      public Slot(int slotId, bool setStackKeyToo = true) {
        stackKey = setStackKeyToo ? slotId : EmptyStackValue;
        containingInventoryType = Types.Get<Type.None>();
        gridLocation = (slotId, 0);
        parentKey = EmptyStackValue;
        otherKey = null;
      }

      /// <summary>
      /// Make a player item slot with player id.
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      /*public Slot(Coordinate? pivot = null, int? stackId = null) {
        gridLocation = pivot ?? (EmptyStackValue, EmptyStackValue);
        containingInventoryType = Types.Get<Type.None>();
        parentKey = null;
        stackKey = stackId ?? EmptyStackValue;
        itemSource = default;
        otherKey = null;
      }*/

      /// <summary>
      /// Make a slot with an id instead of a pivot
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      /*public Slot(Type containingInventory, int barSlotId) {
        stackKey = EmptyStackValue;
        containingInventoryType = containingInventory ?? Types.Get<Type.None>();
        gridLocation = (barSlotId, 0);
        parentKey = EmptyStackValue;
        itemSource = default;
        otherKey = null;
      }

      /// <summary>
      /// Make a slot for an item in a chest in a chunk
      /// </summary>
      /// <param name="pivot"></param>
      /// <param name="containingInventory"></param>
      public Slot(Type chestType, Coordinate chunkAxialKey, int itemStackId, int parentInventoryId) {
        containingInventoryType = chestType ?? Types.Get<Type.None>();
        stackKey = itemStackId;
        gridLocation = chunkAxialKey;
        parentKey = parentInventoryId;
        itemSource = default;
        otherKey = null;
      }*/

      /// <summary>
      /// get from a tuple
      /// </summary>
      public static implicit operator Slot((Type inventory, Coordinate pivot) itemSlotData) 
        => new Slot(itemSlotData.inventory ?? Types.Get<Type.None>(), itemSlotData.pivot);

      /// <summary>
      /// get from a tuple
      /// </summary>
      public static implicit operator Slot(Coordinate pivot) 
        => new Slot(pivot);

      /// <summary>
      /// get from a tuple
      /// </summary>
      public static implicit operator Slot((int, int) pivot) 
        => new Slot(pivot);

      /// <summary>
      /// get from a tuple
      /// </summary>
      public static implicit operator Slot(int barIndex) 
        => new Slot(barIndex);

      /// <summary>
      /// get from a tuple
      /// </summary>
      public static implicit operator Slot((Type inventory, int barSlotId) itemSlotData) 
        => new Slot(itemSlotData.inventory ?? Types.Get<Type.None>(), itemSlotData.barSlotId);

      // ==
      public static bool operator ==(Slot a, Slot b) 
        => a.Equals(b);

      // !=
      public static bool operator !=(Slot a, Slot b) 
        => !(a.Equals(b));

      /// <summary>
      /// an Item = an Item if they can stack and have the same quantity.
      /// </summary>
      public bool Equals(Slot other) 
        => (other.gridLocation == gridLocation)
          && (other.parentKey.Equals(parentKey))
          && (other.containingInventoryType == containingInventoryType)
          && (other.stackKey == stackKey);

      /// <summary>
      /// an Item = an Item if they can stack and have the same quantity.
      /// </summary>
      public override bool Equals(object other) 
        => other is Slot && Equals((Slot)other);

      public override string ToString() 
        => $"Inventory: {containingInventoryType?.Name ?? Types.Get<Type.None>().Name}, gridSlot: {gridLocation}"
          + (!EmptyStackValue.Equals(stackKey) ? $", stackId: {stackKey}" : "")
          + (!EmptyStackValue.Equals(parentKey) ? $", parentId: {parentKey}" : "");
    }
  }

    public interface IOverworldItemSource {

    }
}