﻿using Meep.Tech.Data.Base;
using Meep.Tech.Games.Items;
using Meep.Tech.Games.Items.Components;
using Meep.Tech.Games.Items.Inventories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Meep.Tech.Games.Items.Inventories {
  public interface IInventoryItem
    : IUnique,
      IOwned
  {

    /// <summary>
    /// Combine this and any number of items into one conainer.
    /// </summary>
    public static IInventoryItem Combine(IEnumerable<IInventoryItem> items)
      => items.Count() == 1
        ? items.First()
        : items.Count() > 0
          ? Combine(items.First(), items.Skip(1))
          : null;

    /// <summary>
    /// Combine this and any number of items into one conainer.
    /// </summary>
    public static IInventoryItem Combine(IInventoryItem original, params IInventoryItem[] others)
      => Combine(original, others);

    /// <summary>
    /// Combine this and any number of items into one conainer.
    /// </summary>
    public static IInventoryItem Combine(IInventoryItem original, IEnumerable<IInventoryItem> others) {
      IInventoryItem @return;
      if (original is Item.IBox existingBox) {
        if (others.Count() == 0 && existingBox.quantity == 1) {
          @return = existingBox.First();
        }
        others.ForEach(new System.Action<IInventoryItem>(other =>
          existingBox.addAndExpandWith(other)
        ));

        @return = existingBox;
      } else if (original is Item.Stack originalStack) {
        if (others.Count() == 0 && originalStack.quantity == 1) {
          @return = originalStack.First();
        }
        bool areAllTheSameItem = true;
        IInventoryMultipleItemContainer newBox = new Item.Box(originalStack);
        others.ForEach(other => {
          newBox.addAndExpandWith(other);
          areAllTheSameItem = original.containsTheSameItemsAs(other);
        });
        if (!areAllTheSameItem) {
          @return = newBox;
        } else {
          @return = new Item.Stack(originalStack._baseItem, newBox.quantity);
        }
      } else if (original is Item originalItem) {
        if (others.Count() == 0) {
          return originalItem;
        }

        bool areAllTheSameItem = true;
        IInventoryMultipleItemContainer newBox = new Item.Box(originalItem.AsEnumerable());
        others.ForEach(other => {
          newBox.addAndExpandWith(other);
          areAllTheSameItem = original.containsTheSameItemsAs(other);
        });
        if (!areAllTheSameItem) {
          @return = newBox;
        } else {
          @return = new Item.Stack(originalItem, newBox.quantity);
        }
      } else throw new NotImplementedException($"Cannot combine items unless they're in stacks and boxes or just on their own.");

      if (@return is IInventoryMultipleItemContainer multiItemContainer && multiItemContainer.quantity == 1) {
        return multiItemContainer.First();
      } else return @return;
    }

    /// <summary>
    /// The custom name of the item, if it has one
    /// </summary>
    string name {
      get;
    }

    /// <summary>
    /// The item type of the inventory item.
    /// </summary>
    Item.Type type {
      get;
    }

    /// <summary>
    /// For cloning utility
    /// </summary>
    IInventoryItem clone(bool newUniqueId = true, string newUniqueIdToken = null);

    /// <summary>
    /// Used to set the internal inventory id.
    /// </summary>
    /// <param name="containerInventoryId"></param>
    internal void setContainingInventoryId(string containerInventoryId) {
      owner = containerInventoryId;
    }
  }
}

public static class IInventoryItemFunctions {

  /// <summary>
  /// Can be used to determine if one inventory item contains the same item, or all of the same items if it's a stack or box, as the other provided.
  /// </summary>
  /// <param name="allContentsMustMatch">if the entire inventoryitem, stack, or box's contents must match the type of the given other item. If false only one item in a box needs to match.</param>
  /// <returns>True if it's an item or stack or box where the first item equals, and all items are equal if either are a box.</returns>
  public static bool containsTheSameItemsAs(this IInventoryItem original, IInventoryItem other, bool allContentsMustMatch = true) {
    if (original is IInventoryMultipleItemContainer originalContainer) {
      IInventoryItem firstOriginalItem = originalContainer.First();
      // if the original container was a stack or all the items were the same item in the box:
      if (originalContainer is Item.Stack) {
        if (other is Item) {
          return other.containsTheSameItemsAs(firstOriginalItem);
        } else if (other is IInventoryMultipleItemContainer otherStack) {
          return otherStack.First().containsTheSameItemsAs(firstOriginalItem);
        } else throw new NotImplementedException("Item doesn't seem to be a stack or an item or a box, can't compare it's contents.");
      /// the original container doesn't even have all of the same items:
      // if all contents match:
      } else if (!originalContainer.Any(item => !firstOriginalItem.containsTheSameItemsAs(item))) {
        if (other is Item) {
          return other.containsTheSameItemsAs(firstOriginalItem);
        } else if (other is IInventoryMultipleItemContainer otherStack) {
          return otherStack.First().containsTheSameItemsAs(firstOriginalItem);
        } else throw new NotImplementedException("Item doesn't seem to be a stack or an item or a box, can't compare it's contents.");
        // if it's a box and we don't need all contents to match:
      } else if (!allContentsMustMatch && originalContainer is Item.IBox originalBox) {
        if (other is Item) {
          return originalBox.Any(boxItem => other.containsTheSameItemsAs(boxItem));
        // if the other is a container, check if any of it's contents equal any of this box's contents. 
        } else if (other is IInventoryMultipleItemContainer otherStack) {
          return originalBox.Any(
            boxItem => otherStack.Any(
              stackItem => stackItem.containsTheSameItemsAs(boxItem)
            )
          );
        } else throw new NotImplementedException("Item doesn't seem to be a stack or an item or a box, can't compare it's contents.");
      } else return false;
    } else if (original is Item originalSingleItem) {
      if (other is IInventoryMultipleItemContainer otherContainer) {
        IInventoryItem firstOtherItem = otherContainer.First();
        // if the original container was a stack or all the items were the same item in the box:
        if (otherContainer is Item.Stack) {
          return firstOtherItem.containsTheSameItemsAs(original);
        } // the other container doesn't even have all of the same items, but it can be a box containing just one of the item:
        if (!allContentsMustMatch && otherContainer is Item.IBox otherBox) {
          return otherBox.Any(boxItem => boxItem.containsTheSameItemsAs(originalSingleItem));
        } // if all contents of the other container match
        else if (!otherContainer.Any(item => !firstOtherItem.containsTheSameItemsAs(item))) {
          return true;
        } else return false;
      } else if (other is Item otherSingleItem) {
        return originalSingleItem.Equals(otherSingleItem);
      } // the original container doesn't even have all of the same items:
      else return false;
    } else throw new NotImplementedException("Item doesn't seem to be a stack or an item or a box, can't compare it's contents.");
  }

  /// <summary>
  /// Combine this and any number of items into one conainer.
  /// </summary>
  public static IInventoryItem combine(this IInventoryItem orignal, params IInventoryItem[] others)
    => IInventoryItem.Combine(orignal, others);

  /// <summary>
  /// Combine this and any number of items into one conainer.
  /// </summary>
  public static IInventoryItem combine(this IInventoryItem orignal, IEnumerable<IInventoryItem> others)
    => IInventoryItem.Combine(orignal, others);

  /// <summary>
  /// Get the slot quantity of an inventory item.
  /// Items = 1,
  /// Box or Stacks can have other quantities.
  /// </summary>
  public static int getSlotQuantity(this IInventoryItem inventoryItem)
    => inventoryItem is IInventoryMultipleItemContainer container
      ? container.quantity
      : 1;
}