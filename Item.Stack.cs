﻿using System;
using Meep.Tech.Data.Base;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Meep.Tech.Games.Items.Inventories;
using Meep.Tech.Data.Serialization;
using Meep.Tech.Data.Attributes.Models;

namespace Meep.Tech.Games.Items {

  public partial class Item {

    /// <summary>
    /// A stack of all the same items
    /// </summary>
    [DataModel("Item", typeof(Item.Stack))]
    public struct Stack : IInventoryMultipleItemContainer {

      /// <summary>
      /// The default max stack size
      /// </summary>
      public const int DefaultMaxQuantity = 100;

      internal Item _baseItem;

      public string name 
        => _baseItem.name;

      public int maxQuantity 
        => _baseItem.type.TryToGet<Components.StackQuantityData>()?.MaxStackQuantity ?? DefaultMaxQuantity;

      /// <summary>
      /// If the box is full
      /// </summary>
      public bool isFull
        => quantity >= maxQuantity;

      /// <summary>
      /// how much quantity this can hold more.
      /// </summary>
      public int remainingSpace
        => maxQuantity - quantity;

      public Type type 
        => _baseItem.type;

      public int quantity {
        get;
        internal set;
      } int IInventoryMultipleItemContainer.quantity {
        get => quantity; 
        set => quantity = value; 
      }

      string IUnique.id { 
        get => _baseItem.getUniqueId(); 
        set => throw new NotImplementedException("Cannot set the Item's ID via a Stack");
      }

      string IOwned.owner {
        get => _baseItem.getOwnerId();
        set => throw new NotImplementedException("Cannot set the Item's Owner via a Stack");
      }

      int IInventoryMultipleItemContainer.maxQuantity => throw new NotImplementedException();

      /// <summary>
      /// Make a new stack for a given item
      /// </summary>
      /// <param name="item"></param>
      /// <param name="quantity"></param>
      public Stack(Item item, int quantity) : this() {
        _baseItem = item;
        this.quantity = quantity;
      }

      public bool tryToTakeContents(IInventoryMultipleItemContainer other, out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems) {
        bool allAdded = true;
        Item.Type baseItemType = _baseItem.type;
        List<IInventoryItem> leftovers = new List<IInventoryItem>();
        List<IInventoryItem> successfullyAdded = new List<IInventoryItem>();
        leftoverItems = leftovers;
        successfullyAddedItems = successfullyAdded;
        leftoverQuantity = 0;

        // TODO: do this within the loop below
        if (other.Any(item => !item.type.Equals(baseItemType))) {
          throw new ArrayTypeMismatchException($"Item.Stack of type {baseItemType} tried to add an incorrect item type via add.");
        }

        // if it's a box we need to loop and remove the items from the box:
        if (other is IBox box) {
          // while we have room and the box isn't empty:
          while (box.Count() > 0 && ((maxQuantity - quantity) > 0)) {
            IInventoryItem item = box._contents.Dequeue();
            // if this box item is a stack:
            if (item is Stack stack) {
              box.quantity -= stack.quantity;
              // if we couldn't add the full stack:
              if (!tryToTakeContents(stack, out int leftoverAmount, out var leftoverItem, out var addedItem)) {
                leftoverQuantity += leftoverAmount;
                leftovers.AddRange(leftoverItem);
                // put the leftover back in the box:
                box.tryToAdd(leftoverItem, out _, out _, out _);

                allAdded = false;
              }
              if (addedItem != null) {
                successfullyAdded.AddRange(addedItem);
              }
            } else {
              box.quantity -= 1;
              if (!tryToAdd(out int leftoverAmount, out var leftoverItem, out var addedItem, item)) {
                leftoverQuantity += leftoverAmount;
                leftovers.AddRange(leftoverItem);

                allAdded = false;
              }
              if (addedItem != null) {
                successfullyAdded.AddRange(addedItem);
              }
            }
          }
        } // if it's a stack it's easy 
        else {
          int remainingSpace;
          // if there's space left:
          if ((remainingSpace = maxQuantity - quantity) > 0) {
            if (remainingSpace <= other.quantity) {
              quantity += other.quantity;
              successfullyAddedItems = (Item.Stack)other.clone();
              other.quantity = 0;
            } // partial remaining space, just add what fits:
            else {
              quantity = maxQuantity;
              successfullyAddedItems = other.split(remainingSpace);
              //other.quantity -= remainingSpace;
              leftoverQuantity += other.quantity;
              leftovers.Add(other);

              allAdded = false;
            }
          }
        }

        return allAdded;
      }

      public bool tryToAdd(IEnumerable<IInventoryItem> items, out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems) {
        List<IInventoryItem> leftovers = new List<IInventoryItem>();
        List<IInventoryItem> successfullyAdded = new List<IInventoryItem>();
        leftoverQuantity = 0;
        bool allAdded = true;
        leftoverItems = leftovers;
        successfullyAddedItems = successfullyAdded;

        foreach (var item in items) {
          if (item is Stack stack) {
            int remainingSpace;
            // if there's space left:
            if ((remainingSpace = maxQuantity - quantity) > 0) {
              if (remainingSpace <= stack.quantity) {
                quantity += stack.quantity;
                successfullyAdded.Add(item);
              } // partial remaining space, just add what fits:
              else {
                quantity = maxQuantity;
                successfullyAdded.Add(stack.split(remainingSpace));
                leftoverQuantity += stack.quantity;
                leftovers.Add(stack);
                allAdded = false;
              }
            } // no remaining space, just return the stack:
            else {
              leftoverQuantity += stack.quantity;
              leftovers.Add(stack);
              allAdded = false;
            }

            continue;
          } else if (item is Box) throw new InvalidOperationException($"BOX WITHIN A STACK! Cannot add a Box to a Stack as a sub-item. Try takeContents instead.");

          if (maxQuantity - quantity > 0) {
            quantity++;
            successfullyAdded.Add(item);
          } else {
            leftoverQuantity++;
            leftovers.Add(item);
            allAdded = false;
          }
        }

        return allAdded;
      }

      public bool tryToAdd(out int leftoverQuantity, out IEnumerable<IInventoryItem> leftoverItems, out IEnumerable<IInventoryItem> successfullyAddedItems, params IInventoryItem[] items)
        => tryToAdd(items, out leftoverQuantity, out leftoverItems, out successfullyAddedItems);

      public IInventoryMultipleItemContainer split(int desiredResultQuantity) {
        int quantityToRemove = desiredResultQuantity > quantity ? quantity : quantity - desiredResultQuantity;
        quantity -= quantityToRemove;
        return new Item.Stack(_baseItem.clone(), desiredResultQuantity);
      }

      public IInventoryItem clone(bool newUniqueId = true, string newUniqueIdToken = null)
        => new Stack(_baseItem.clone(), quantity);

      public IEnumerator<IInventoryItem> GetEnumerator()
        => _baseItem.AsEnumerable().GetEnumerator();

      IEnumerator IEnumerable.GetEnumerator()
        => GetEnumerator();

      IInventoryMultipleItemContainer IInventoryMultipleItemContainer.addAndExpandWith(IInventoryItem inventoryItem) {
        int potentialQuantity = quantity + inventoryItem.getSlotQuantity();
        IInventoryMultipleItemContainer @return = this;
        // if it overloads the stack, wrap the stack in a box, then add the items:
        if (potentialQuantity > maxQuantity) {
          @return = new Item.Box(this);
          @return = @return.addAndExpandWith(inventoryItem);
        // if we have room and it's a stack, add it
        } else if (inventoryItem is IInventoryMultipleItemContainer container) {
          tryToTakeContents(container, out _, out _, out _);
        // else if it's a normal item just add it:
        } else tryToAdd(out _, out _, out _, inventoryItem);
        return @return;
      }
    }
  }
}