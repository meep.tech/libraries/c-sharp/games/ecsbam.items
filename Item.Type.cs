﻿using Meep.Tech.Data;
using System.Collections.Generic;
using System.Linq;
using Meep.Tech.Data.Base;
using Meep.Tech.Data.Extensions;
using Meep.Tech.Games.Items.Inventories;
using Meep.Tech.Data.Serialization;
using Newtonsoft.Json.Linq;
using Meep.Tech.Data.Attributes.Archetypes;

namespace Meep.Tech.Games.Items {

  public partial class Item {

    #region Archetype Collection

    /// <summary>
    /// The item types collection
    /// </summary>
    public static Archetype.Collection<Item, Type, Type.Id> Types {
      get;
    } = new Archetype.Collection<Item, Type, Type.Id>();

    #endregion

    /// <summary>
    /// A basic Item Type, for non-stackable items
    /// </summary>
    public abstract partial class Type : Archetype<Item, Type, Type.Id> {

      /// <summary>
      /// How many of this item can you hold in one slot
      /// </summary>
      [ArchetypeInfoField]
      public string Description {
        get;
        protected set;
      } = "An Item";

      /// <summary>
      /// The base GP cost of this item
      /// </summary>
      // TODO: make this a Currency struct after you make ITradeable
      [ArchetypeInfoField]
      public int BaseValue {
        get;
        protected set;
      } = 5;

      #region Initialization

      protected Type(Id id) : base(id, Types) {}

      public override OrderdDictionary<Archetype.DataComponent.Id, IArchetypeDataComponent> DefaultDataComponents
        => base.DefaultDataComponents
          .Append(new Components.StackQuantityData());

      #region ModelConstruction

      protected override Item InitializeModel(Model.Params @params = null) 
        => new Item(this);

      protected override Item ConfigureModel(Item model, Model.Params @params = null) {
        model = base.ConfigureModel(model, @params);
        model.name = @params.GetAs(Item.Params.CustomName, Name);

        return model;
      }

      /// <summary>
      /// Make an item stack model of the given size with an optional UUID if one exists
      /// </summary>
      protected internal Item ReMake(string uuid) {
        return base.Make(new Dictionary<Param, object> {
          { Param.UniqueId, uuid}
        });
      }

      /// <summary>
      /// Make an item
      /// </summary>
      public Stack Make(string creatorToken, int quantity) {
        return
          Make(
            quantity,
            new Dictionary<Param, object> {
              { Param.CreatorToken, creatorToken}
            }
          );
      }

      /// <summary>
      /// Make an item
      /// </summary>
      public Item Make(string creatorToken) {
        return
          base.Make(
            new Dictionary<Param, object> {
              { Param.CreatorToken, creatorToken}
            }
          );
      }

      /// <summary>
      /// Make an item
      /// </summary>
      public Stack Make(int quantity, Dictionary<Param, object> @params = null) {
        return new Stack(
          base.Make(@params),
          quantity
        );
      }

      /// <summary>
      /// Make an item
      /// </summary>
      public Stack MakeStack(Dictionary<Param, object> @params = null, int quantity = 1) {
        return new Stack(
          base.Make(@params),
          quantity
        );
      }

      /// <summary>
      /// Make an item or a stack of them
      /// </summary>
      public static IInventoryItem Make(Serializer.SerializedData serializedData) {
        Item itemBase = Item.Types.Make(serializedData);
        if (serializedData.jsonData.TryGetValue(Params.StackQuantity.Name, out JToken value)) {
          int? stackQuantity = value.Value<int?>() ?? itemBase.type.TryToGet<Components.StackQuantityData>()?.DefaultStackQuantity;
          if (stackQuantity.HasValue) {
            return new Stack(
              itemBase,
              stackQuantity.Value
            );
          }
        }

        return itemBase;
      }

      /// <summary>
      /// Make an item or a stack of them from a collection. This accounts for box data stored with the items.
      /// </summary>
      public static IEnumerable<IInventoryItem> Make(IEnumerable<Serializer.SerializedData> serializedDatas) {
        Dictionary<string, IInventoryItem> deserializedItems 
          = new Dictionary<string, IInventoryItem>();

        foreach(Serializer.SerializedData data in serializedDatas) { 
          if (data.jsonData.TryGetValue(IBox.Params.BoxId.Name, out JToken serializedBoxId)) {
            string boxId = serializedBoxId.Value<string>();
            if (deserializedItems.TryGetValue(boxId, out IInventoryItem boxItem) && boxItem is IBox box) {
              box.tryToAdd(out _, out _, out _, Item.Types.Make(data));
              deserializedItems[boxId] = box; 
            } else {
              deserializedItems[boxId] = new Box(
                boxItem.AsEnumerable(),
                data.jsonData.Value<int>(IBox.Params.MaxSize)
              );
            }
          }
        }

        return deserializedItems.Values;
      }

      /// <summary>
      /// Make an item or a stack of them
      /// </summary>
      public new IInventoryItem Make(Dictionary<Param, object> @params = null) {
        Item itemBase = base.Make(@params);
        int? stackQuantity;
        if ((stackQuantity = @params.GetAs(Params.StackQuantity, itemBase.type.TryToGet<Components.StackQuantityData>()?.DefaultStackQuantity))
          != null
        ) {
          return new Stack(
            itemBase,
            stackQuantity.Value
          );
        }

        return itemBase;
      }

      #endregion

      #endregion
    }
  }
}