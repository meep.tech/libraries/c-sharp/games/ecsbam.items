﻿using System;
using Meep.Tech.Data.Base;
using Meep.Tech.Games.Items.Inventories;
using Meep.Tech.Data;
using Meep.Tech.Data.Attributes.Models;

namespace Meep.Tech.Games.Items {

  /// <summary>
  /// Represents an Single item, placeable in an inventory or dropable into the world, etc
  /// </summary>
  [DataModel("Item", typeof(Item.Type))]
  public partial class Item : 
    Model<Item, Item.Type, Item.Type.Id>,
    IInventoryItem,
    IEquatable<Item>,
    ICached<Item>
  {

    #region Model Data

    /// <summary>
    /// Params for the base item construction
    /// </summary>
    public new class Params : Param {

      /// <summary>
      /// Used to initialize a stack instead of just on item.
      /// Passing in null will try to use the default stack size.
      /// </summary>
      public static Params StackQuantity {
        get;
      } = new Item.Params("Stack Quantity", typeof(int?));

      /// <summary>
      /// A custom item name
      /// </summary>
      public static Params CustomName {
        get;
      } = new Params("Name", typeof(string));

      protected internal Params(string name, System.Type type, string externalIdEnding = null)
        : base(name, type, $"Items.{externalIdEnding ?? name}") { }
    }

    /// <summary>
    /// The UUID of the item
    /// </summary>
    [field: ModelIdField]
    string IUnique.id {
      get;
      set;
    }

    /// <summary>
    /// The custom name of the item, if it has one
    /// </summary>
    [ModelDataField]
    public string name {
      get;
      protected set;
    }

    /// <summary>
    /// Owner of the inventory
    /// </summary>
    [ModelOwnerField]
    public string owner {
      get;
      internal set;
    } string IOwned.owner {
      get => owner;
      set => owner = value;
    }

    #endregion

    /// <summary>
    /// The value in GP of this item
    /// TODO: move to sellable component?
    /// </summary>
    public virtual int value {
      get => type.BaseValue;
    }

    #region Initialization

    /// <summary>
    /// Make a new item/stack of item
    /// </summary>
    protected Item(Type type) : base(type) {}

    /// <summary>
    /// Copy an item:
    /// </summary>
    public override Item clone(bool newUniqueId = true, string newUniqueIdToken = null) {
      return type.ReMake(newUniqueId ? RNG.GetNextUniqueIdFromToken(newUniqueIdToken) : this.getUniqueId());
    }

    IInventoryItem IInventoryItem.clone(bool newUniqueId, string newUniqueIdToken)
      => clone(); 
    
    #endregion

    #region Serialization

    /// <summary>
    /// Item basic deets
    /// </summary>
    /// <returns></returns>
    public override string ToString() {
      return $"Item::[{type.Name}]";
    }

    /// <summary>
    /// Hash code from id
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode() {
      return this.getUniqueId().GetHashCode();
    }

    #endregion
  }
}