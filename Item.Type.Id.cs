﻿using Meep.Tech.Data.Base;

namespace Meep.Tech.Games.Items {

  public partial class Item {

    public partial class Type {

      /// <summary>
      /// item ids
      /// </summary>
      public new class Id : Identity {

        /// <summary>
        /// No item Id 
        /// </summary>
        public static Id None {
          get;
        } = new Id("None");

        public Id(string name, string externalIdNameEnding = null)
          : base(name, $"Meep.Tech.Games.Items.{externalIdNameEnding ?? ""}") { }
      }
    }
  }
}
