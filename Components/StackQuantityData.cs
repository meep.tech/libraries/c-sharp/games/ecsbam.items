﻿using Meep.Tech.Data.Base;

namespace Meep.Tech.Games.Items.Components {
  public class StackQuantityData : Archetype.DataComponent<StackQuantityData> {
    public new class Type : Archetype.DataComponent.Type<StackQuantityData> {
      public static new Archetype.DataComponent.Id Id {
        get;
      } = new Archetype.DataComponent.Id("Stackable", "Items.");
      Type() : base(Id) {}
    }

    /// <summary>
    /// The Max stack quantity default for this item type.
    /// </summary>
    public int MaxStackQuantity {
      get;
    }

    /// <summary>
    /// The default stack quantity default for this item type.
    /// </summary>
    public int DefaultStackQuantity {
      get;
    }

    public StackQuantityData(int maxStackQuantity = Item.Stack.DefaultMaxQuantity, int defaultStackQuantity = 1)
      : base(Type.GetInstance<Type>()) 
    {
      DefaultStackQuantity = defaultStackQuantity;
      MaxStackQuantity = maxStackQuantity;
    }
  }
}
